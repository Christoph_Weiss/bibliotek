/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Views;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_startmenu extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JLabel lb_title;

    private JButton btn_ausleihen_gui;
    private JButton btn_person;
    private JButton btn_buch;
    private JButton btn_zurueckgeben;
    private JButton btn_verlag;
    private JButton btn_autor;

    public Gui_startmenu() {
        initalize();
    }

    private void initalize() {
        this.setSize(new Dimension(650, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("Eins gute GUI");
        mainPanel = new JPanel();

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("Bibliothek");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(225, 5, 500, 70);
        mainPanel.add(lb_title);

        File f1 = new File("");
        String path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\buchausleihen.png";
        Icon img = new ImageIcon(path);
        btn_ausleihen_gui = new JButton(img);

        btn_ausleihen_gui.setOpaque(false);
        btn_ausleihen_gui.setContentAreaFilled(false);
        btn_ausleihen_gui.setBorderPainted(false);
        btn_ausleihen_gui.setBorder(BorderFactory.createEmptyBorder());
        btn_ausleihen_gui.setBounds(100, 150, 100, 100);
        btn_ausleihen_gui.addActionListener(this);
        mainPanel.add(btn_ausleihen_gui);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\person.png";
        img = new ImageIcon(path);
        btn_person = new JButton(img);

        btn_person.setOpaque(false);
        btn_person.setContentAreaFilled(false);
        btn_person.setBorderPainted(false);
        btn_person.setBorder(BorderFactory.createEmptyBorder());
        btn_person.setBounds(100, 300, 100, 100);
        btn_person.addActionListener(this);
        mainPanel.add(btn_person);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\buch.png";
        img = new ImageIcon(path);
        btn_buch = new JButton(img);

        btn_buch.setOpaque(false);
        btn_buch.setContentAreaFilled(false);
        btn_buch.setBorderPainted(false);
        btn_buch.setBorder(BorderFactory.createEmptyBorder());
        btn_buch.setBounds(275, 150, 100, 100);
        btn_buch.addActionListener(this);
        mainPanel.add(btn_buch);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\buchzurueckgeben.png";
        img = new ImageIcon(path);
        btn_zurueckgeben = new JButton(img);

        btn_zurueckgeben.setOpaque(false);
        btn_zurueckgeben.setContentAreaFilled(false);
        btn_zurueckgeben.setBorderPainted(false);
        btn_zurueckgeben.setBorder(BorderFactory.createEmptyBorder());
        btn_zurueckgeben.setBounds(275, 300, 100, 100);
        btn_zurueckgeben.addActionListener(this);
        mainPanel.add(btn_zurueckgeben);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\autor.png";
        img = new ImageIcon(path);
        btn_autor = new JButton(img);

        btn_autor.setOpaque(false);
        btn_autor.setContentAreaFilled(false);
        btn_autor.setBorderPainted(false);
        btn_autor.setBorder(BorderFactory.createEmptyBorder());
        btn_autor.setBounds(425, 150, 100, 100);
        btn_autor.addActionListener(this);
        mainPanel.add(btn_autor);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\verlag.png";
        img = new ImageIcon(path);
        btn_verlag = new JButton(img);

        btn_verlag.setOpaque(false);
        btn_verlag.setContentAreaFilled(false);
        btn_verlag.setBorderPainted(false);
        btn_verlag.setBorder(BorderFactory.createEmptyBorder());
        btn_verlag.setBounds(425, 300, 100, 100);
        btn_verlag.addActionListener(this);
        mainPanel.add(btn_verlag);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn_ausleihen_gui) {
            System.out.println("ausleihen");
            Gui_Ausleihen_Zurückgeben ga = new Gui_Ausleihen_Zurückgeben();
            ga.setVisible(true);
        } else if (e.getSource() == btn_autor) {
            System.out.println("autor");
            Gui_Autor ga = new Gui_Autor();
            ga.setVisible(true);
        } else if (e.getSource() == btn_buch) {
            System.out.println("buch");
            Gui_bucheigenschaften g = new Gui_bucheigenschaften();
            g.setVisible(true);
        } else if (e.getSource() == btn_person) {
            System.out.println("person");
            Gui_Person gp= new Gui_Person();
            gp.setVisible(true);
        } else if (e.getSource() == btn_verlag) {
            System.out.println("verlag");
            Gui_Verlag gv = new Gui_Verlag();
            gv.setVisible(true);
        } else if (e.getSource() == btn_zurueckgeben) {
            System.out.println("zurueckgeben");
            Gui_zurueckgeben gz = new Gui_zurueckgeben();
            gz.setVisible(true);
        }

    }
}
