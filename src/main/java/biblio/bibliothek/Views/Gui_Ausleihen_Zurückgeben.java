/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Views;

import biblio.bibliothek.Classes.Ausleihen;
import biblio.bibliothek.Classes.Autor;
import biblio.bibliothek.Classes.Buch;
import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Person;
import biblio.bibliothek.Classes.Verlag;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_Ausleihen_Zurückgeben extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JLabel lb_title;
    private JLabel lb_buchnummer;
    private JLabel lb_sachgebiet;
    private JLabel lb_buchtitel;
    private JLabel lb_autor_nachname;
    private JLabel lb_verlag;
    private JLabel lb_ausweisnummer;
    private JLabel lb_personname;
    private JLabel lb_ort;
    private JLabel lb_personid;
    private JLabel lb_erscheinungsJahr;
    private JLabel lb_anzahldatensaetze;
    private JLabel lb_aktuelles_Buch;

    private JTextField tf_buchnummer;
    private JTextField tf_sachgebiet;
    private JTextField tf_buchtitel;
    private JTextField tf_ort;
    private JTextField tf_erscheinungsJahr;
    private JTextField tf_autor;
    private JTextField tf_verlag;
    private JTextField tf_ausweisnummer;
    private JTextField tf_personid;
    private JTextField tf_personname;
    private JTextField tf_ausleih_id;
    private JTextField tf_suchen;

    private JButton btn_newData;
    private JButton btn_search;
    private JButton btn_edit;
    private JButton btn_delete;
    private JButton btn_back;
    private JButton btn_backward;
    private JButton btn_forward;
    private JButton btn_execute;
    private JButton btn_update;
    private JButton btn_zurueckgeben;
    private JButton btn_suche;
    private JButton btn_reset;
    
    private JComboBox buch_combobox;
    private JComboBox person_combobox;

    private ArrayList<Buch> buchlist = new ArrayList();
    private ArrayList<Verlag> verlaglist = new ArrayList();
    private ArrayList<Autor> autorlist = new ArrayList();
    private ArrayList<Ausleihen> verleihlist = new ArrayList();
    private ArrayList<Person> personlist = new ArrayList();
    private ArrayList<Buch> ausleihbar = new ArrayList();
    
    private int status_id;
    private int anzahl_Datensaetze;
    private int help;
    
    private String search_ein;

    private boolean hidden;
    private boolean update;
    private boolean search;

    public Gui_Ausleihen_Zurückgeben() {
        initalize();
        fill_JTextFields();
        this.repaint();
        this.revalidate();
    }

    private void initalize() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("Ausleihen");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(200, 5, 500, 50);
        mainPanel.add(lb_title);
        
        tf_suchen = new JTextField();
        tf_suchen.setBounds(500,5,100,25);
        tf_suchen.setVisible(false);
        mainPanel.add(tf_suchen);
        
        btn_suche = new JButton();
        btn_suche.setText("GO");
        btn_suche.setVisible(false);
        btn_suche.addActionListener(this);
        btn_suche.setBounds(600, 5,70, 25);
        mainPanel.add(btn_suche);
        
        btn_reset = new JButton();
        btn_reset.setText("Reset");
        btn_reset.setVisible(false);
        btn_reset.addActionListener(this);
        btn_reset.setBounds(600, 50,70, 25);
        mainPanel.add(btn_reset);

        buch_combobox = new JComboBox();
        buch_combobox.setEnabled(false);
        buch_combobox.setFont(new Font("Serif", Font.PLAIN, 15));
        buch_combobox.setBounds(200, 80, 225, 25);
        mainPanel.add(buch_combobox);

        lb_buchnummer = new JLabel();
        lb_buchnummer.setText("Buchnummer");
        lb_buchnummer.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_buchnummer.setBounds(50, 140, 150, 25);
        mainPanel.add(lb_buchnummer);

        tf_buchnummer = new JTextField();
        tf_buchnummer.setEditable(false);
        tf_buchnummer.setBounds(150, 140, 150, 25);
        mainPanel.add(tf_buchnummer);

        lb_sachgebiet = new JLabel();
        lb_sachgebiet.setText("Sachgebiet");
        lb_sachgebiet.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_sachgebiet.setBounds(320, 140, 150, 25);
        mainPanel.add(lb_sachgebiet);

        tf_sachgebiet = new JTextField();
        tf_sachgebiet.setBounds(400, 140, 150, 25);
        tf_sachgebiet.setEditable(false);
        mainPanel.add(tf_sachgebiet);

        lb_buchtitel = new JLabel();
        lb_buchtitel.setText("Buchitel");
        lb_buchtitel.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_buchtitel.setBounds(50, 180, 200, 25);
        mainPanel.add(lb_buchtitel);

        tf_buchtitel = new JTextField();
        tf_buchtitel.setBounds(150, 180, 150, 25);
        tf_buchtitel.setEditable(false);
        mainPanel.add(tf_buchtitel);

        lb_autor_nachname = new JLabel();
        lb_autor_nachname.setText("Autor");
        lb_autor_nachname.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_autor_nachname.setBounds(320, 180, 150, 25);
        mainPanel.add(lb_autor_nachname);

        tf_autor = new JTextField();
        tf_autor.setBounds(400, 180, 150, 25);
        tf_autor.setEditable(false);
        mainPanel.add(tf_autor);

        lb_verlag = new JLabel();
        lb_verlag.setText("Verlag");
        lb_verlag.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_verlag.setBounds(50, 220, 150, 25);
        mainPanel.add(lb_verlag);

        tf_verlag = new JTextField();
        tf_verlag.setBounds(150, 220, 150, 25);
        tf_verlag.setEditable(false);
        mainPanel.add(tf_verlag);

        lb_ort = new JLabel();
        lb_ort.setText("Ort");
        lb_ort.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_ort.setBounds(320, 220, 150, 25);
        mainPanel.add(lb_ort);

        tf_ort = new JTextField();
        tf_ort.setBounds(400, 220, 150, 25);
        tf_ort.setEditable(false);
        mainPanel.add(tf_ort);

        lb_erscheinungsJahr = new JLabel();
        lb_erscheinungsJahr.setText("Erscheinungsjahr");
        lb_erscheinungsJahr.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_erscheinungsJahr.setBounds(50, 260, 150, 25);
        mainPanel.add(lb_erscheinungsJahr);

        tf_erscheinungsJahr = new JTextField();
        tf_erscheinungsJahr.setEditable(false);
        tf_erscheinungsJahr.setBounds(180, 260, 150, 25);
        mainPanel.add(tf_erscheinungsJahr);

        person_combobox = new JComboBox();
        person_combobox.setEnabled(false);
        person_combobox.setFont(new Font("Serif", Font.PLAIN, 15));
        person_combobox.setBounds(200, 300, 200, 25);
        mainPanel.add(person_combobox);

        lb_personid = new JLabel();
        lb_personid.setText("Person ID:");
        lb_personid.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_personid.setBounds(50, 340, 150, 25);
        mainPanel.add(lb_personid);

        tf_personid = new JTextField();
        tf_personid.setEditable(false);
        tf_personid.setBounds(180, 340, 150, 25);
        mainPanel.add(tf_personid);

        lb_personname = new JLabel();
        lb_personname.setText("Personenname");
        lb_personname.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_personname.setBounds(340, 340, 150, 25);
        mainPanel.add(lb_personname);

        tf_personname = new JTextField();
        tf_personname.setBounds(450, 340, 150, 25);
        tf_personname.setEditable(false);
        mainPanel.add(tf_personname);

        lb_ausweisnummer = new JLabel();
        lb_ausweisnummer.setBounds(50, 380, 150, 25);
        lb_ausweisnummer.setText("Ausweissnummer");
        lb_ausweisnummer.setFont(new Font("Serif", Font.PLAIN, 15));
        mainPanel.add(lb_ausweisnummer);

        tf_ausweisnummer = new JTextField();
        tf_ausweisnummer.setBounds(180, 380, 150, 25);
        tf_ausweisnummer.setEditable(false);
        mainPanel.add(tf_ausweisnummer);
        
        tf_ausleih_id = new JPasswordField();
        tf_ausleih_id.setVisible(false);
        mainPanel.add(tf_ausleih_id);

        btn_backward = new JButton();
        btn_backward.setText("←");
        btn_backward.setBounds(450, 425, 60, 60);
        btn_backward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_backward.addActionListener(this);
        mainPanel.add(btn_backward);
        btn_forward = new JButton();
        btn_forward.setText("→");
        btn_forward.setBounds(525, 425, 60, 60);
        btn_forward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_forward.addActionListener(this);
        mainPanel.add(btn_forward);

        lb_anzahldatensaetze = new JLabel();
        lb_anzahldatensaetze.setBounds(645, 425, 20, 40);
        mainPanel.add(lb_anzahldatensaetze);

        lb_aktuelles_Buch = new JLabel();
        lb_aktuelles_Buch.setBounds(620, 425, 20, 40);
        mainPanel.add(lb_aktuelles_Buch);

        File f1 = new File("");
        String path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\newData.png";
        ImageIcon img = new ImageIcon(path);
        btn_newData = new JButton(img);

        btn_newData.setOpaque(false);
        btn_newData.setContentAreaFilled(false);
        btn_newData.setBorderPainted(false);
        btn_newData.setBorder(BorderFactory.createEmptyBorder());
        btn_newData.setBounds(150, 500, 50, 50);
        btn_newData.addActionListener(this);
        mainPanel.add(btn_newData);
        
        btn_zurueckgeben = new JButton();
        btn_zurueckgeben.setText("Zurückgeben");
        btn_zurueckgeben.setBounds(340, 380, 200, 25);
        btn_zurueckgeben.addActionListener(this);
        mainPanel.add(btn_zurueckgeben);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\search.png";
        img = new ImageIcon(path);
        btn_search = new JButton(img);

        btn_search.setOpaque(false);
        btn_search.setContentAreaFilled(false);
        btn_search.setBorderPainted(false);
        btn_search.setEnabled(false);
        btn_search.setBorder(BorderFactory.createEmptyBorder());
        btn_search.setBounds(250, 500, 50, 50);
        btn_search.addActionListener(this);
        mainPanel.add(btn_search);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\edit.png";
        img = new ImageIcon(path);
        btn_edit = new JButton(img);

        btn_edit.setBorder(BorderFactory.createEmptyBorder());
        btn_edit.setOpaque(false);
        btn_edit.setEnabled(false);
        btn_edit.setContentAreaFilled(false);
        btn_edit.setBorderPainted(false);
        btn_edit.addActionListener(this);
        btn_edit.setBounds(350, 500, 50, 50);
        mainPanel.add(btn_edit);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\delete.png";
        img = new ImageIcon(path);
        btn_delete = new JButton(img);
        btn_delete.setBorder(BorderFactory.createEmptyBorder());
        btn_delete.setOpaque(false);
        btn_delete.setEnabled(false);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorderPainted(false);
        btn_delete.setBounds(450, 500, 50, 50);
        btn_delete.addActionListener(this);
        mainPanel.add(btn_delete);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\back.png";
        img = new ImageIcon(path);
        btn_back = new JButton(img);

        btn_back.setBorder(BorderFactory.createEmptyBorder());
        btn_back.setOpaque(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setBorderPainted(false);
        btn_back.addActionListener(this);
        btn_back.setBounds(550, 500, 50, 50);
        mainPanel.add(btn_back);

        btn_execute = new JButton();
        btn_execute.setText("Speichern");
        btn_execute.setBounds(440, 425, 200, 20);
        btn_execute.addActionListener(this);
        btn_execute.setVisible(false);
        mainPanel.add(btn_execute);

        btn_update = new JButton();
        btn_update.setText("Aktualisieren");
        btn_update.setBounds(440, 425, 200, 20);
        btn_update.addActionListener(this);
        btn_update.setVisible(false);
        mainPanel.add(btn_update);

    }

    public void hide() {
        if (hidden) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            btn_execute.setVisible(true);
            btn_update.setVisible(false);
            tf_buchnummer.setText("");
            tf_buchtitel.setText("");
            tf_erscheinungsJahr.setText("");
            tf_sachgebiet.setText("");
            tf_ort.setText("");
            tf_autor.setText("");
            tf_verlag.setText("");
            tf_personid.setText("");
            tf_personname.setText("");
            tf_ausweisnummer.setText("");
            tf_autor.setText("");
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(false);
            tf_erscheinungsJahr.setEditable(false);
            tf_sachgebiet.setEditable(false);
            tf_ort.setEditable(false);
            tf_autor.setEditable(false);
            tf_verlag.setEditable(false);
            tf_personid.setEditable(false);
            tf_personname.setEditable(false);
            tf_ausweisnummer.setEditable(false);
            tf_autor.setEditable(false);
            buch_combobox.setEnabled(true);
            person_combobox.setEnabled(true);
            btn_zurueckgeben.setVisible(false);

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_buchnummer.setVisible(true);
            tf_buchnummer.setVisible(true);
            btn_update.setVisible(false);
            btn_execute.setVisible(false);
            buch_combobox.setEnabled(false);
            person_combobox.setEnabled(false);
            btn_zurueckgeben.setVisible(true);
            fill_JTextFields();
        }
    }

    public void getArraylistFromResultSet() {
        try {
            DBConnection db = new DBConnection();
            Connection conn = db.getConnection();
            Buch b = new Buch();
            Verlag v = new Verlag();
            Autor at = new Autor();
            Ausleihen as = new Ausleihen();
            Person p = new Person();
            personlist = new ArrayList();
            buchlist = new ArrayList();
            autorlist = new ArrayList();
            verlaglist = new ArrayList();
            verleihlist = new ArrayList();
            ResultSet rs = null;
            
            if(search){
               //verleihlist = as.ausleihenAllSearch(search_ein);
                System.out.println(personlist.size());
            }else{
             rs = as.ausleihenAll();
            while (rs.next()) {
                //id,vorname,nachname,ausweisnummer,email,adresse
                Ausleihen a = new Ausleihen();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setPerson_id(Integer.parseInt(rs.getObject(2).toString()));
                a.setBuch_id(Integer.parseInt(rs.getObject(3).toString()));
                a.setAusleihdatum(rs.getObject(4).toString());
                a.setZurueckgabedatum(rs.getObject(5).toString());
                verleihlist.add(a);
            }
            rs.close();
            }
            rs = b.getAllBuecher();
            //sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,buchnummer
            while (rs.next()) {
                Buch a = new Buch();
                a.setBuchnummer(Integer.parseInt(rs.getObject(7).toString()));
                a.setBuchtitel(rs.getObject(2).toString());
                a.setErscheinungsjahr((rs.getObject(4).toString()));
                a.setOrt(rs.getObject(3).toString());
                a.setSachgebiet(rs.getObject(1).toString());
                a.setVerlag_id(Integer.parseInt(rs.getObject(5).toString()));
                a.setAutor_id(Integer.parseInt(rs.getObject(6).toString()));
                buchlist.add(a);

            }
            rs.close();
            rs = v.getAllVerlaege();
            while (rs.next()) {

                Verlag a = new Verlag();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setName(rs.getObject(2).toString());
                a.setOrt(rs.getObject(3).toString());
                verlaglist.add(a);
            }
            rs.close();
            rs = at.getAllAutor();
            while (rs.next()) {
                //id,vorname,nachname,geburtsort,geburtsdatum
                Autor a = new Autor();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setGeburtsort(rs.getObject(4).toString());
                a.setGeburtsdatum(rs.getObject(5).toString());
                autorlist.add(a);
            }
            rs.close();

            rs = p.getAllPerson();
            while (rs.next()) {
                //id,vorname,nachname,ausweisnummer,email,adresse
                Person a = new Person();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setAusweisnummer(Integer.parseInt(rs.getObject(4).toString()));
                a.setEmail(rs.getObject(5).toString());
                a.setAdresse(rs.getObject(6).toString());
                personlist.add(a);
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    public void fill_JTextFields() {
        getArraylistFromResultSet();
        anzahl_Datensaetze = verleihlist.size();
        if (anzahl_Datensaetze < 1) {
            for (int i = 0; i < personlist.size(); i++) {
                Person v = personlist.get(i);
                person_combobox.addItem(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
            }
            for (int i = 0; i < buchlist.size(); i++) {
                Buch v = buchlist.get(i);
                buch_combobox.addItem(v.getBuchtitel());
            }
            tf_buchnummer.setText("");
            tf_buchtitel.setText("");
            tf_erscheinungsJahr.setText("");
            tf_sachgebiet.setText("");
            tf_ort.setText("");
            tf_ausleih_id.setText("");
            tf_ausweisnummer.setText("");
            tf_autor.setText("");
            tf_personid.setText("");
            tf_personname.setText("");
            tf_verlag.setText("");
            
           

            person_combobox.setEnabled(false);
            buch_combobox.setEnabled(false);
            btn_zurueckgeben.setEnabled(false);

        }
        if (anzahl_Datensaetze > 0) {
            btn_zurueckgeben.setEnabled(true);
            status_id = 0;
            lb_anzahldatensaetze.setText("" + anzahl_Datensaetze);
            help = status_id;
            help++;
            lb_aktuelles_Buch.setText("" + help);
            Ausleihen b = verleihlist.get(0);
            tf_ausleih_id.setText("" + b.getId());
            person_combobox.removeAllItems();
            for (int i = 0; i < personlist.size(); i++) {
                Person v = personlist.get(i);
                person_combobox.addItem(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
            }
            for (int i = 0; i < personlist.size(); i++) {
                Person v = personlist.get(i);
                if (v.getId() == b.getPerson_id()) {
                    tf_personid.setText("" + v.getId());
                    tf_ausweisnummer.setText("" + v.getAusweisnummer());
                    tf_personname.setText(v.getVorname() + " " + v.getNachname());
                    person_combobox.setSelectedItem(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
                    this.repaint();
                }
            }
            buch_combobox.removeAllItems();
            for (int i = 0; i < buchlist.size(); i++) {
                Buch v = buchlist.get(i);
                buch_combobox.addItem(v.getBuchtitel());
            }
            for (int i = 0; i < buchlist.size(); i++) {
                Buch v = buchlist.get(i);
                if (v.getBuchnummer() == b.getBuch_id()) {
                    Verlag a = new Verlag();
                    tf_verlag.setText(a.getVerlag(v.getVerlag_id()).getName());
                    Autor ab = new Autor();
                    tf_autor.setText(ab.getAutor(v.getAutor_id()).getVorname() + " " + ab.getAutor(v.getAutor_id()).getNachname());
                    buch_combobox.setSelectedItem(v.getBuchtitel());
                    tf_buchnummer.setText("" + v.getBuchnummer());
                    tf_buchtitel.setText(v.getBuchtitel());
                    tf_sachgebiet.setText(v.getSachgebiet());
                    tf_ort.setText(v.getOrt());
                    tf_erscheinungsJahr.setText(v.getErscheinungsjahr());
                }
            }

        }

    }

    public void fill_JTextFields(int id) {
        Ausleihen b = verleihlist.get(id);
        tf_ausleih_id.setText("" + b.getId());
        help = id;
        lb_aktuelles_Buch.setText("" + ++help);

        for (int i = 0; i < personlist.size(); i++) {
            Person v = personlist.get(i);
            if (v.getId() == b.getPerson_id()) {
                tf_personid.setText("" + v.getId());
                tf_ausweisnummer.setText("" + v.getAusweisnummer());
                tf_personname.setText(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
                person_combobox.setSelectedItem(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
                this.repaint();
            }
        }

        for (int i = 0; i < buchlist.size(); i++) {
            Buch v = buchlist.get(i);
            if (v.getBuchnummer() == b.getBuch_id()) {
                Verlag a = new Verlag();
                tf_verlag.setText(a.getVerlag(v.getVerlag_id()).getName());
                Autor ab = new Autor();
                tf_autor.setText(ab.getAutor(v.getAutor_id()).getVorname() + " " + ab.getAutor(v.getAutor_id()).getNachname());
                buch_combobox.setSelectedItem(v.getBuchtitel());
                tf_buchnummer.setText("" + v.getBuchnummer());
                tf_buchtitel.setText(v.getBuchtitel());
                tf_sachgebiet.setText(v.getSachgebiet());
                tf_ort.setText(v.getOrt());
                tf_erscheinungsJahr.setText(v.getErscheinungsjahr());
            }
        }
    }

    public void update() {
        if (update) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_buchnummer.setVisible(false);
            tf_buchnummer.setVisible(false);
            btn_update.setVisible(true);
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(true);
            tf_erscheinungsJahr.setEditable(true);
            tf_sachgebiet.setEditable(true);
            tf_ort.setEditable(true);
            person_combobox.setEnabled(true);
            buch_combobox.setEnabled(true);

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            btn_update.setVisible(false);
            lb_buchnummer.setVisible(true);
            tf_buchnummer.setVisible(true);
            btn_update.setVisible(false);
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(false);
            tf_erscheinungsJahr.setEditable(false);
            tf_sachgebiet.setEditable(false);
            tf_ort.setEditable(false);
            person_combobox.setEnabled(false);
            buch_combobox.setEnabled(false);

            fill_JTextFields();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn_forward) {
            if ((anzahl_Datensaetze - 1) > status_id) {
                status_id++;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_backward) {
            if (status_id > 0) {
                status_id--;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_delete) {
            if (JOptionPane.showConfirmDialog(null, "Soll dieses Buch gelöscht werden?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                try {
                    Buch help = buchlist.get(status_id);
                    Buch b = new Buch();
                    Ausleihen a = new Ausleihen();
                    a.deleteAusleihen(help);
                    b.deleteBuchWithObject(help);
                    fill_JTextFields();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "help " + ex);
                }
            }
        } else if (e.getSource() == btn_newData) {
            Buch as = new Buch();
            ausleihbar = as.getAllNotAusgeliehen();
            if(ausleihbar.size() <=0){
                JOptionPane.showMessageDialog(null, "Derzeit ist kein Buch ausleihbar!!!");
            }else{
                
            buch_combobox.removeAllItems();
            for (int i = 0; i < ausleihbar.size(); i++) {
                Buch v = ausleihbar.get(i);
                buch_combobox.addItem(v.getBuchtitel());
            }
            buch_combobox.setSelectedItem(ausleihbar.get(0));
                hidden = true;
                hide();
            }
            person_combobox.removeAllItems();
            for (int i = 0; i < personlist.size(); i++) {
                Person v = personlist.get(i);
                person_combobox.addItem(v.getVorname() + " " + v.getNachname() + " " + v.getAusweisnummer());
            }
            person_combobox.setSelectedItem(personlist.get(0));
            
        } else if (e.getSource() == btn_execute) {
            int counter = 0;
            Buch l = new Buch();
            Person a = new Person();
            for (int i = 0; i < ausleihbar.size(); i++) {
                String buch = ausleihbar.get(i).getBuchtitel();
                if (buch_combobox.getSelectedItem() == buch) {
                    l.setBuchnummer(ausleihbar.get(i).getBuchnummer());
                    l.setBuchtitel(ausleihbar.get(i).getBuchtitel());
                    l.setSachgebiet(ausleihbar.get(i).getSachgebiet());
                    l.setVerlag_id(ausleihbar.get(i).getVerlag_id());
                    l.setOrt(ausleihbar.get(i).getOrt());
                    l.setErscheinungsjahr(ausleihbar.get(i).getErscheinungsjahr());

                }

            }
            for (int i = 0; i < personlist.size(); i++) {
                String person = personlist.get(i).getVorname() + " " + personlist.get(i).getNachname() + " " + personlist.get(i).getAusweisnummer();
                if (person_combobox.getSelectedItem().equals(person)) {
                    a.setId(personlist.get(i).getId());
                    a.setVorname(personlist.get(i).getVorname());
                    a.setNachname(personlist.get(i).getNachname());
                    a.setAusweisnummer(personlist.get(i).getAusweisnummer());
                    a.setEmail(personlist.get(i).getEmail());
                    a.setAdresse(personlist.get(i).getAdresse());

                }

            }
            Ausleihen b = new Ausleihen();
            try {
                b.insertBuchausleihen(l, a);
            } catch (SQLException ex) {
                counter++;
                ex.printStackTrace();
                //JOptionPane.showMessageDialog(null, ex);
            }
            if (counter == 1) {
                hidden = true;
            } else {
                hidden = false;
            }
            hide();
        } else if (e.getSource() == btn_edit) {
            update = true;
            update();
        } else if (e.getSource() == btn_update) {
//            int counter = 0;
////            try {
//            Buch l = new Buch();
//            Person a = new Person();
//            Ausleihen b = new Ausleihen();
//
//            for (int i = 0; i < buchlist.size(); i++) {
//                String buch = buchlist.get(i).getBuchtitel();
//                if (buch_combobox.getSelectedItem() == buch) {
//                    l.setBuchnummer(buchlist.get(i).getBuchnummer());
//                    l.setBuchtitel(buchlist.get(i).getBuchtitel());
//                    l.setSachgebiet(buchlist.get(i).getSachgebiet());
//                    l.setVerlag_id(buchlist.get(i).getVerlag_id());
//                    l.setOrt(buchlist.get(i).getOrt());
//                    l.setErscheinungsjahr(buchlist.get(i).getErscheinungsjahr());
//
//                }
//
//            }
//            for (int i = 0; i < personlist.size(); i++) {
//                String person = personlist.get(i).getVorname() + " " + personlist.get(i).getNachname();
//                if (person_combobox.getSelectedItem().equals(person)) {
//                    a.setId(personlist.get(i).getId());
//                    a.setVorname(personlist.get(i).getVorname());
//                    a.setNachname(personlist.get(i).getNachname());
//                    a.setAusweisnummer(personlist.get(i).getAusweisnummer());
//                    a.setEmail(personlist.get(i).getEmail());
//                    a.setAdresse(personlist.get(i).getAdresse());
//
//                }
//
//            }
//
////                try {
////                    b.updateBuch(tf_sachgebiet.getText(), tf_buchtitel.getText(), tf_ort.getText(), tf_erscheinungsJahr.getText(), l.getId(), a.getId(), Integer.parseInt(tf_buchnummer.getText()));
////                } catch (ParseException ex) {
////                    JOptionPane.showMessageDialog(null, ex);
////                }
////            } catch (SQLException ex) {
////                JOptionPane.showMessageDialog(null, ex);
////                counter++;
////            }
////            if (counter == 1) {
////                update = true;
////            } else {
////                update = false;
////            }
////            update();
        } else if (e.getSource() == btn_back) {
            hidden = false;
            hide();
        }else if(e.getSource() == btn_zurueckgeben){
               int counter = 0;
            Buch l = new Buch();
            Person a = new Person();
            for (int i = 0; i < buchlist.size(); i++) {
                String buch = buchlist.get(i).getBuchtitel();
                if (buch_combobox.getSelectedItem() == buch) {
                    l.setBuchnummer(buchlist.get(i).getBuchnummer());
                    l.setBuchtitel(buchlist.get(i).getBuchtitel());
                    l.setSachgebiet(buchlist.get(i).getSachgebiet());
                    l.setVerlag_id(buchlist.get(i).getVerlag_id());
                    l.setOrt(buchlist.get(i).getOrt());
                    l.setErscheinungsjahr(buchlist.get(i).getErscheinungsjahr());

                }

            }
            for (int i = 0; i < personlist.size(); i++) {
                String person = personlist.get(i).getVorname() + " " + personlist.get(i).getNachname();
                if (person_combobox.getSelectedItem().equals(person)) {
                    a.setId(personlist.get(i).getId());
                    a.setVorname(personlist.get(i).getVorname());
                    a.setNachname(personlist.get(i).getNachname());
                    a.setAusweisnummer(personlist.get(i).getAusweisnummer());
                    a.setEmail(personlist.get(i).getEmail());
                    a.setAdresse(personlist.get(i).getAdresse());

                }

            }
            Ausleihen b = new Ausleihen();
            try {
                b.updateBuchzurueckgeben(Integer.parseInt(tf_ausleih_id.getText()),l.getBuchnummer());
            } catch (SQLException ex) {
                counter++;
                ex.printStackTrace();
                //JOptionPane.showMessageDialog(null, ex);
            }
            if (counter == 1) {
                hidden = true;
            } else {
                hidden = false;
            }
            hide();
        }
    }

}
