/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Views;

import biblio.bibliothek.Classes.Ausleihen;
import biblio.bibliothek.Classes.Autor;
import biblio.bibliothek.Classes.Buch;
import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Verlag;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_bucheigenschaften extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JLabel lb_title;
    private JLabel lb_buchnummer;
    private JLabel lb_sachgebiet;
    private JLabel lb_buchtitel;
    private JLabel lb_autor_nachname;
    private JLabel lb_verlag;
    private JLabel lb_ort;
    private JLabel lb_erscheinungsJahr;
    private JLabel lb_anzahldatensaetze;
    private JLabel lb_aktuelles_Buch;

    private JTextField tf_buchnummer;
    private JTextField tf_sachgebiet;
    private JTextField tf_buchtitel;
    private JTextField tf_ort;
    private JTextField tf_erscheinungsJahr;
    private JTextField tf_suchen;

    private JButton btn_newData;
    private JButton btn_suche;
    private JButton btn_reset;
    private JButton btn_search;
    private JButton btn_edit;
    private JButton btn_delete;
    private JButton btn_back;
    private JButton btn_backward;
    private JButton btn_forward;
    private JButton btn_execute;
    private JButton btn_update;

    private JComboBox verlag_combobox;
    private JComboBox autor_combobox;

    private ArrayList<Buch> buchlist = new ArrayList();
    private ArrayList<Verlag> verlaglist = new ArrayList();
    private ArrayList<Autor> autorlist = new ArrayList();

    private int status_id;
    private int anzahl_Datensaetze;
    private int help;
    private String search_ein;

    private boolean hidden;
    private boolean update;
    private boolean search;

    public Gui_bucheigenschaften() {
        initalize();
        fill_JTextFields();
        this.repaint();
        this.revalidate();
    }
//table.setDefaultEditor(Object.class, null);

    private void initalize() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("Buch-Eigenschaften");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(200, 5, 500, 70);
        mainPanel.add(lb_title);
        
        tf_suchen = new JTextField();
        tf_suchen.setBounds(500,5,100,25);
        tf_suchen.setVisible(false);
        mainPanel.add(tf_suchen);
        
        btn_suche = new JButton();
        btn_suche.setText("GO");
        btn_suche.setVisible(false);
        btn_suche.addActionListener(this);
        btn_suche.setBounds(600, 5,70, 25);
        mainPanel.add(btn_suche);
        
        btn_reset = new JButton();
        btn_reset.setText("Reset");
        btn_reset.setVisible(false);
        btn_reset.addActionListener(this);
        btn_reset.setBounds(600, 50,70, 25);
        mainPanel.add(btn_reset);

        lb_buchnummer = new JLabel();
        lb_buchnummer.setText("Buchnummer");
        lb_buchnummer.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_buchnummer.setBounds(50, 80, 150, 25);
        mainPanel.add(lb_buchnummer);

        tf_buchnummer = new JTextField();
        tf_buchnummer.setEditable(false);
        tf_buchnummer.setBounds(150, 80, 500, 25);
        mainPanel.add(tf_buchnummer);

        lb_sachgebiet = new JLabel();
        lb_sachgebiet.setText("Sachgebiet");
        lb_sachgebiet.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_sachgebiet.setBounds(50, 140, 200, 25);
        mainPanel.add(lb_sachgebiet);

        tf_sachgebiet = new JTextField();
        tf_sachgebiet.setBounds(150, 140, 500, 25);
        tf_sachgebiet.setEditable(false);
        mainPanel.add(tf_sachgebiet);

        lb_buchtitel = new JLabel();
        lb_buchtitel.setText("Buchitel");
        lb_buchtitel.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_buchtitel.setBounds(50, 200, 200, 25);
        mainPanel.add(lb_buchtitel);

        tf_buchtitel = new JTextField();
        tf_buchtitel.setBounds(150, 200, 500, 25);
        tf_buchtitel.setEditable(false);
        mainPanel.add(tf_buchtitel);

        lb_autor_nachname = new JLabel();
        lb_autor_nachname.setText("Autor");
        lb_autor_nachname.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_autor_nachname.setBounds(50, 260, 150, 25);
        mainPanel.add(lb_autor_nachname);

        autor_combobox = new JComboBox();
        autor_combobox.setEnabled(false);
        autor_combobox.setFont(new Font("Serif", Font.PLAIN, 15));
        autor_combobox.setBounds(170, 260, 225, 25);
        mainPanel.add(autor_combobox);

        lb_verlag = new JLabel();
        lb_verlag.setText("Verlag");
        lb_verlag.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_verlag.setBounds(50, 320, 200, 25);
        mainPanel.add(lb_verlag);

        verlag_combobox = new JComboBox();
        verlag_combobox.setEnabled(false);
        verlag_combobox.setFont(new Font("Serif", Font.PLAIN, 15));
        verlag_combobox.setBounds(100, 320, 200, 25);
        mainPanel.add(verlag_combobox);
        System.out.println(verlag_combobox.getItemCount());

        lb_ort = new JLabel();
        lb_ort.setText("Ort");
        lb_ort.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_ort.setBounds(350, 320, 200, 25);
        mainPanel.add(lb_ort);

        tf_ort = new JTextField();
        tf_ort.setBounds(400, 320, 200, 25);
        tf_ort.setEditable(false);
        mainPanel.add(tf_ort);

        lb_erscheinungsJahr = new JLabel();
        lb_erscheinungsJahr.setText("Erscheinungsjahr");
        lb_erscheinungsJahr.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_erscheinungsJahr.setBounds(50, 380, 200, 25);
        mainPanel.add(lb_erscheinungsJahr);

        tf_erscheinungsJahr = new JTextField();
        tf_erscheinungsJahr.setEditable(false);
        tf_erscheinungsJahr.setBounds(180, 380, 100, 25);
        mainPanel.add(tf_erscheinungsJahr);

        btn_backward = new JButton();
        btn_backward.setText("←");
        btn_backward.setBounds(450, 425, 60, 60);
        btn_backward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_backward.addActionListener(this);
        mainPanel.add(btn_backward);
        btn_forward = new JButton();
        btn_forward.setText("→");
        btn_forward.setBounds(525, 425, 60, 60);
        btn_forward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_forward.addActionListener(this);
        mainPanel.add(btn_forward);

        lb_anzahldatensaetze = new JLabel();
        lb_anzahldatensaetze.setBounds(645, 425, 20, 40);
        mainPanel.add(lb_anzahldatensaetze);

        lb_aktuelles_Buch = new JLabel();
        lb_aktuelles_Buch.setBounds(620, 425, 20, 40);
        mainPanel.add(lb_aktuelles_Buch);

        File f1 = new File("");
        String path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\newData.png";
        ImageIcon img = new ImageIcon(path);
        btn_newData = new JButton(img);

        btn_newData.setOpaque(false);
        btn_newData.setContentAreaFilled(false);
        btn_newData.setBorderPainted(false);
        btn_newData.setBorder(BorderFactory.createEmptyBorder());
        btn_newData.setBounds(150, 500, 50, 50);
        btn_newData.addActionListener(this);
        mainPanel.add(btn_newData);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\search.png";
        img = new ImageIcon(path);
        btn_search = new JButton(img);

        btn_search.setOpaque(false);
        btn_search.setContentAreaFilled(false);
        btn_search.setBorderPainted(false);
        btn_search.setBorder(BorderFactory.createEmptyBorder());
        btn_search.setBounds(250, 500, 50, 50);
        btn_search.addActionListener(this);
        mainPanel.add(btn_search);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\edit.png";
        img = new ImageIcon(path);
        btn_edit = new JButton(img);

        btn_edit.setBorder(BorderFactory.createEmptyBorder());
        btn_edit.setOpaque(false);
        btn_edit.setContentAreaFilled(false);
        btn_edit.setBorderPainted(false);
        btn_edit.addActionListener(this);
        btn_edit.setBounds(350, 500, 50, 50);
        mainPanel.add(btn_edit);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\delete.png";
        img = new ImageIcon(path);
        btn_delete = new JButton(img);
        btn_delete.setBorder(BorderFactory.createEmptyBorder());
        btn_delete.setOpaque(false);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorderPainted(false);
        btn_delete.setBounds(450, 500, 50, 50);
        btn_delete.addActionListener(this);
        mainPanel.add(btn_delete);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\back.png";
        img = new ImageIcon(path);
        btn_back = new JButton(img);

        btn_back.setBorder(BorderFactory.createEmptyBorder());
        btn_back.setOpaque(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setBorderPainted(false);
        btn_back.addActionListener(this);
        btn_back.setBounds(550, 500, 50, 50);
        mainPanel.add(btn_back);

        btn_execute = new JButton();
        btn_execute.setText("Speichern");
        btn_execute.setBounds(440, 425, 200, 20);
        btn_execute.addActionListener(this);
        btn_execute.setVisible(false);
        mainPanel.add(btn_execute);

        btn_update = new JButton();
        btn_update.setText("Aktualisieren");
        btn_update.setBounds(440, 425, 200, 20);
        btn_update.addActionListener(this);
        btn_update.setVisible(false);
        mainPanel.add(btn_update);

    }

    public void hide() {
        if (hidden) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_buchnummer.setVisible(false);
            tf_buchnummer.setVisible(false);
            btn_execute.setVisible(true);
            btn_update.setVisible(false);
            tf_buchnummer.setText("");
            tf_buchtitel.setText("");
            tf_erscheinungsJahr.setText("");
            tf_sachgebiet.setText("");
            tf_ort.setText("");
            tf_buchnummer.setEditable(true);
            tf_buchtitel.setEditable(true);
            tf_erscheinungsJahr.setEditable(true);
            tf_sachgebiet.setEditable(true);
            tf_ort.setEditable(true);
            verlag_combobox.setEnabled(true);
            autor_combobox.setEnabled(true);

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_buchnummer.setVisible(true);
            tf_buchnummer.setVisible(true);
            btn_update.setVisible(false);
            btn_execute.setVisible(false);
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(false);
            tf_erscheinungsJahr.setEditable(false);
            tf_sachgebiet.setEditable(false);
            tf_ort.setEditable(false);
            verlag_combobox.setEnabled(false);
            autor_combobox.setEnabled(false);
            fill_JTextFields();
        }
    }

    public void getArraylistFromResultSet() {
        long timeStart = System.currentTimeMillis();
        try {
            DBConnection db = new DBConnection();
            Connection conn = db.getConnection();
            Buch b = new Buch();
            Verlag v = new Verlag();
            Autor at = new Autor();
            buchlist = new ArrayList();
            autorlist = new ArrayList();
            verlaglist = new ArrayList();
            ResultSet rs = null;
            if(search){
               buchlist = b.getAllBuecherSearch(search_ein);
                System.out.println(buchlist.size());
            }else{
             rs = b.getAllBuecher();
            //sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,buchnummer
            while (rs.next()) {
                Buch a = new Buch();
                a.setBuchnummer(Integer.parseInt(rs.getObject(7).toString()));
                a.setBuchtitel(rs.getObject(2).toString());
                a.setErscheinungsjahr((rs.getObject(4).toString()));
                a.setOrt(rs.getObject(3).toString());
                a.setSachgebiet(rs.getObject(1).toString());
                a.setVerlag_id(Integer.parseInt(rs.getObject(5).toString()));
                a.setAutor_id(Integer.parseInt(rs.getObject(6).toString()));
                buchlist.add(a);

            }
            rs.close();
            }
            rs = v.getAllVerlaege();
            while (rs.next()) {

                Verlag a = new Verlag();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setName(rs.getObject(2).toString());
                a.setOrt(rs.getObject(3).toString());
                verlaglist.add(a);
            }
            rs.close();
            rs = at.getAllAutor();
            while (rs.next()) {
                //id,vorname,nachname,geburtsort,geburtsdatum
                Autor a = new Autor();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setGeburtsort(rs.getObject(4).toString());
                a.setGeburtsdatum(rs.getObject(5).toString());
                autorlist.add(a);
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        
        System.out.println(System.currentTimeMillis() - timeStart);

    }

    public void fill_JTextFields() {
        getArraylistFromResultSet();
        anzahl_Datensaetze = buchlist.size();
        //System.out.println("Anzahl Datensätze" + anzahl_Datensaetze);
        if (anzahl_Datensaetze < 1) {
            for (int i = 0; i < autorlist.size(); i++) {
                Autor v = autorlist.get(i);
                autor_combobox.addItem(v.getVorname() + " " + v.getNachname());
            }
            for (int i = 0; i < verlaglist.size(); i++) {
                Verlag v = verlaglist.get(i);
                verlag_combobox.addItem(v.getName());
            }
            tf_buchnummer.setText("");
            tf_buchtitel.setText("");
            tf_erscheinungsJahr.setText("");
            tf_sachgebiet.setText("");
            tf_ort.setText("");

            verlag_combobox.setEnabled(false);
            autor_combobox.setEnabled(false);

        }
        if (anzahl_Datensaetze > 0) {

            status_id = 0;
            lb_anzahldatensaetze.setText("" + anzahl_Datensaetze);
            help = status_id;
            help++;
            lb_aktuelles_Buch.setText("" + help);
            Buch b = buchlist.get(0);

            tf_buchnummer.setText("" + b.getBuchnummer());
            tf_buchtitel.setText(b.getBuchtitel());
            tf_sachgebiet.setText(b.getSachgebiet());
            tf_ort.setText(b.getOrt());
            tf_erscheinungsJahr.setText(b.getErscheinungsjahr());
            verlag_combobox.removeAllItems();
            for (int i = 0; i < verlaglist.size(); i++) {
                
                Verlag v = verlaglist.get(i);
                verlag_combobox.addItem(v.getName());
            }
            System.out.println(verlag_combobox.getItemCount());
            for (int i = 0; i < verlaglist.size(); i++) {
                Verlag v = verlaglist.get(i);
                if (v.getId() == b.getVerlag_id()) {
                    verlag_combobox.setSelectedItem(v.getName());
                    this.repaint();
                }
            }
            autor_combobox.removeAllItems();
            for (int i = 0; i < autorlist.size(); i++) {
                Autor v = autorlist.get(i);
                autor_combobox.addItem(v.getVorname() + " " + v.getNachname());
            }
            for (int i = 0; i < autorlist.size(); i++) {
                Autor v = autorlist.get(i);
                if (v.getId() == b.getAutor_id()) {
                    autor_combobox.setSelectedItem(v.getVorname() + " " + v.getNachname());
                    this.repaint();
                }
            }
        }


    }

    public void fill_JTextFields(int id) {
        Buch b = buchlist.get(id);
        help = id;
        lb_aktuelles_Buch.setText("" + ++help);
        tf_buchnummer.setText("" + b.getBuchnummer());
        tf_buchtitel.setText(b.getBuchtitel());
        tf_sachgebiet.setText(b.getSachgebiet());
        tf_ort.setText(b.getOrt());
        tf_erscheinungsJahr.setText(b.getErscheinungsjahr());
        for (int i = 0; i < verlaglist.size(); i++) {
            Verlag v = verlaglist.get(i);
            if (v.getId() == b.getVerlag_id()) {
                verlag_combobox.setSelectedItem(v.getName());
                this.repaint();
            }
        }
        for (int i = 0; i < autorlist.size(); i++) {
            Autor a = autorlist.get(i);
            if (a.getId() == b.getAutor_id()) {
                autor_combobox.setSelectedItem(a.getVorname() + " " + a.getNachname());
                this.repaint();
            }
        }
    }

    public void update() {
        if (update) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_buchnummer.setVisible(false);
            tf_buchnummer.setVisible(false);
            btn_update.setVisible(true);
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(true);
            tf_erscheinungsJahr.setEditable(true);
            tf_sachgebiet.setEditable(true);
            tf_ort.setEditable(true);
            verlag_combobox.setEnabled(true);
            autor_combobox.setEnabled(true);

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_buchnummer.setVisible(true);
            tf_buchnummer.setVisible(true);
            btn_update.setVisible(false);
            tf_buchnummer.setEditable(false);
            tf_buchtitel.setEditable(false);
            tf_erscheinungsJahr.setEditable(false);
            tf_sachgebiet.setEditable(false);
            tf_ort.setEditable(false);
            verlag_combobox.setEnabled(false);
            autor_combobox.setEnabled(false);
            btn_reset.setVisible(false);

            fill_JTextFields();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn_forward) {
            if ((anzahl_Datensaetze - 1) > status_id) {
                status_id++;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_backward) {
            if (status_id > 0) {
                status_id--;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_delete) {
            if (JOptionPane.showConfirmDialog(null, "Soll dieses Buch gelöscht werden?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                try {
                    Buch help = buchlist.get(status_id);
                    Buch b = new Buch();
                    Ausleihen a = new Ausleihen();
                    a.deleteAusleihen(help);
                    b.deleteBuchWithObject(help);
                    fill_JTextFields();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "help " + ex);
                }
            }
        } else if (e.getSource() == btn_newData) {
            hidden = true;
            hide();
        } else if (e.getSource() == btn_execute) {
            int counter = 0;
            if((tf_sachgebiet.getText().isEmpty() || tf_buchtitel.getText().isEmpty() || tf_ort.getText().isEmpty() || tf_erscheinungsJahr.getText().isEmpty()) == false){
            try {
                Verlag l = new Verlag();
                Autor a = new Autor();
                for (int i = 0; i < verlaglist.size(); i++) {
                    String verlag = verlaglist.get(i).getName();
                    if (verlag_combobox.getSelectedItem() == verlag) {
                        l.setName(verlaglist.get(i).getName());

                        l.setOrt(verlaglist.get(i).getOrt());
                        l.setId(verlaglist.get(i).getId());
                    }

                }
                for (int i = 0; i < autorlist.size(); i++) {
                    String autor = autorlist.get(i).getVorname() + " " + autorlist.get(i).getNachname();
                    if (autor_combobox.getSelectedItem().equals(autor)) {
                        a.setId(autorlist.get(i).getId());
                        a.setVorname(autorlist.get(i).getVorname());
                        a.setNachname(autorlist.get(i).getNachname());
                        a.setGeburtsort(autorlist.get(i).getGeburtsort());
                        a.setGeburtsdatum(autorlist.get(i).getGeburtsdatum());
                    }

                }
                Buch b = new Buch();
                b.insertBuch(tf_sachgebiet.getText(), tf_buchtitel.getText(), tf_ort.getText(), tf_erscheinungsJahr.getText(), l.getId(), a.getId(),false);

            } catch (ParseException ex) {
                ex.printStackTrace();
                //JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                hidden = true;
            } else {
                hidden = false;
            }
            hide();
            }else{
                JOptionPane.showMessageDialog(null, "Füllen sie alle Felder aus!");
            }
        } else if (e.getSource() == btn_edit) {
            update = true;
            update();
        } else if (e.getSource() == btn_update) {
            int counter = 0;
            try {
                Verlag l = new Verlag();
                Autor a = new Autor();
                Buch b = new Buch();
                for (int i = 0; i < verlaglist.size(); i++) {
                    String verlag = verlaglist.get(i).getName();
                    if (verlag_combobox.getSelectedItem() == verlag) {
                        l.setName(verlaglist.get(i).getName());
                        l.setOrt(verlaglist.get(i).getOrt());
                        l.setId(verlaglist.get(i).getId());
                    }

                }
                for (int i = 0; i < autorlist.size(); i++) {
                    String autor = autorlist.get(i).getVorname() + " " + autorlist.get(i).getNachname();
                    if (autor_combobox.getSelectedItem().equals(autor)) {
                        a.setId(autorlist.get(i).getId());
                        a.setVorname(autorlist.get(i).getVorname());
                        a.setNachname(autorlist.get(i).getNachname());
                        a.setGeburtsort(autorlist.get(i).getGeburtsort());
                        a.setGeburtsdatum(autorlist.get(i).getGeburtsdatum());
                    }

                }

                try {
                    b.updateBuch(tf_sachgebiet.getText(), tf_buchtitel.getText(), tf_ort.getText(), tf_erscheinungsJahr.getText(), l.getId(), a.getId(), Integer.parseInt(tf_buchnummer.getText()));
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                update = true;
            } else {
                update = false;
            }
            update();
        } else if (e.getSource() == btn_back) {
            hidden = false;
            hide();
        }else if(e.getSource() == btn_suche){
            
            System.out.println(tf_suchen.getText().isEmpty());
            if(tf_suchen.getText().isEmpty() == false){
            btn_suche.setVisible(false);
            tf_suchen.setVisible(false);
            btn_reset.setVisible(true);
            search = true;
                System.out.println(tf_suchen.getText());
            search_ein = tf_suchen.getText();
            }
            fill_JTextFields();
            search = false;
        }else if(e.getSource() == btn_search){
            btn_suche.setVisible(true);
            tf_suchen.setVisible(true);
            tf_suchen.requestFocus();
        }else if(e.getSource() == btn_reset){
           tf_suchen.setText("");
           fill_JTextFields();
           btn_reset.setVisible(false);
        }
    }

}
