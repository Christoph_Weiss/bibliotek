/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Views;

import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Person;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_Person extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JLabel lb_title;
    private JLabel lb_id;
    private JLabel lb_vorname;
    private JLabel lb_nachname;
    private JLabel lb_adresse;
    private JLabel lb_email;
    private JLabel lb_ausweisnummer;
    private JLabel lb_anzahldatensaetze;
    private JLabel lb_aktuelles_Buch;
    

    private JTextField tf_id;
    private JTextField tf_vorname;
    private JTextField tf_nachname;
    private JTextField tf_adresse;
    private JTextField tf_email;
    private JTextField tf_ausweisnummer;
    private JTextField tf_suchen;

    private JButton btn_newData;
    private JButton btn_search;
    private JButton btn_edit;
    private JButton btn_delete;
    private JButton btn_back;
    private JButton btn_backward;
    private JButton btn_forward;
    private JButton btn_execute;
    private JButton btn_update;
    private JButton btn_suche;
    private JButton btn_reset;

   
    private ArrayList<Person> personlist = new ArrayList();

    private int status_id;
    private int anzahl_Datensaetze;
    private int help;
    
    private String search_ein;

    private boolean hidden;
    private boolean update;
    private boolean search;

    public Gui_Person() {
        initalize();
        fill_JTextFields();
    }


    private void initalize() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("Person/Schüler");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(200, 5, 500, 70);
        mainPanel.add(lb_title);
        
        tf_suchen = new JTextField();
        tf_suchen.setBounds(500,5,100,25);
        tf_suchen.setVisible(false);
        mainPanel.add(tf_suchen);
        
        btn_suche = new JButton();
        btn_suche.setText("GO");
        btn_suche.setVisible(false);
        btn_suche.addActionListener(this);
        btn_suche.setBounds(600, 5,70, 25);
        mainPanel.add(btn_suche);
        
        btn_reset = new JButton();
        btn_reset.setText("Reset");
        btn_reset.setVisible(false);
        btn_reset.addActionListener(this);
        btn_reset.setBounds(600, 50,70, 25);
        mainPanel.add(btn_reset);

        lb_id = new JLabel();
        lb_id.setText("ID:");
        lb_id.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_id.setBounds(50, 80, 150, 25);
        mainPanel.add(lb_id);

        tf_id = new JTextField();
        tf_id.setEditable(false);
        tf_id.setBounds(150, 80, 500, 25);
        mainPanel.add(tf_id);

        lb_vorname = new JLabel();
        lb_vorname.setText("Vorname");
        lb_vorname.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_vorname.setBounds(50, 140, 200, 25);
        mainPanel.add(lb_vorname);

        tf_vorname = new JTextField();
        tf_vorname.setBounds(150, 140, 500, 25);
        tf_vorname.setEditable(false);
        mainPanel.add(tf_vorname);

        lb_nachname = new JLabel();
        lb_nachname.setText("Nachname");
        lb_nachname.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_nachname.setBounds(50, 200, 200, 25);
        mainPanel.add(lb_nachname);

        tf_nachname = new JTextField();
        tf_nachname.setBounds(150, 200, 500, 25);
        tf_nachname.setEditable(false);
        mainPanel.add(tf_nachname);

        lb_adresse = new JLabel();
        lb_adresse.setText("Adresse");
        lb_adresse.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_adresse.setBounds(50, 260, 150, 25);
        mainPanel.add(lb_adresse);

        tf_adresse = new JTextField();
        tf_adresse.setBounds(150, 260, 200, 25);
        tf_adresse.setEditable(false);
        mainPanel.add(tf_adresse);
        
        lb_email = new JLabel();
        lb_email.setText("Email");
        lb_email.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_email.setBounds(50, 320, 500, 25);
        mainPanel.add(lb_email);

        tf_email = new JTextField();
        tf_email.setBounds(150, 320, 300, 25);
        tf_email.setEditable(false);
        mainPanel.add(tf_email);
        
        lb_ausweisnummer = new JLabel();
        lb_ausweisnummer.setText("Ausweisnummer");
        lb_ausweisnummer.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_ausweisnummer.setBounds(50, 380, 200, 25);
        mainPanel.add(lb_ausweisnummer);

        tf_ausweisnummer = new JTextField();
        tf_ausweisnummer.setBounds(175, 380, 150, 25);
        tf_ausweisnummer.setEditable(false);
        mainPanel.add(tf_ausweisnummer);

        btn_backward = new JButton();
        btn_backward.setText("←");
        btn_backward.setBounds(450, 425, 60, 60);
        btn_backward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_backward.addActionListener(this);
        mainPanel.add(btn_backward);
        
        btn_forward = new JButton();
        btn_forward.setText("→");
        btn_forward.setBounds(525, 425, 60, 60);
        btn_forward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_forward.addActionListener(this);
        mainPanel.add(btn_forward);

        lb_anzahldatensaetze = new JLabel();
        lb_anzahldatensaetze.setBounds(645, 425, 20, 40);
        mainPanel.add(lb_anzahldatensaetze);

        lb_aktuelles_Buch = new JLabel();
        lb_aktuelles_Buch.setBounds(620, 425, 20, 40);
        mainPanel.add(lb_aktuelles_Buch);

        File f1 = new File("");
        String path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\newData.png";
        ImageIcon img = new ImageIcon(path);
        btn_newData = new JButton(img);

        btn_newData.setOpaque(false);
        btn_newData.setContentAreaFilled(false);
        btn_newData.setBorderPainted(false);
        btn_newData.setBorder(BorderFactory.createEmptyBorder());
        btn_newData.setBounds(150, 500, 50, 50);
        btn_newData.addActionListener(this);
        mainPanel.add(btn_newData);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\search.png";
        img = new ImageIcon(path);
        btn_search = new JButton(img);

        btn_search.setOpaque(false);
        btn_search.setContentAreaFilled(false);
        btn_search.setBorderPainted(false);
        btn_search.setBorder(BorderFactory.createEmptyBorder());
        btn_search.setBounds(250, 500, 50, 50);
        btn_search.addActionListener(this);
        mainPanel.add(btn_search);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\edit.png";
        img = new ImageIcon(path);
        btn_edit = new JButton(img);

        btn_edit.setBorder(BorderFactory.createEmptyBorder());
        btn_edit.setOpaque(false);
        btn_edit.setContentAreaFilled(false);
        btn_edit.setBorderPainted(false);
        btn_edit.addActionListener(this);
        btn_edit.setBounds(350, 500, 50, 50);
        mainPanel.add(btn_edit);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\delete.png";
        img = new ImageIcon(path);
        btn_delete = new JButton(img);
        btn_delete.setBorder(BorderFactory.createEmptyBorder());
        btn_delete.setOpaque(false);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorderPainted(false);
        btn_delete.setBounds(450, 500, 50, 50);
        btn_delete.addActionListener(this);
        mainPanel.add(btn_delete);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\back.png";
        img = new ImageIcon(path);
        btn_back = new JButton(img);

        btn_back.setBorder(BorderFactory.createEmptyBorder());
        btn_back.setOpaque(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setBorderPainted(false);
        btn_back.addActionListener(this);
        btn_back.setBounds(550, 500, 50, 50);
        mainPanel.add(btn_back);

        btn_execute = new JButton();
        btn_execute.setText("Speichern");
        btn_execute.setBounds(440, 425, 200, 20);
        btn_execute.addActionListener(this);
        btn_execute.setVisible(false);
        mainPanel.add(btn_execute);

        btn_update = new JButton();
        btn_update.setText("Aktualisieren");
        btn_update.setBounds(440, 425, 200, 20);
        btn_update.addActionListener(this);
        btn_update.setVisible(false);
        mainPanel.add(btn_update);

    }

    public void hide() {
        if (hidden) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_id.setVisible(false);
            tf_id.setVisible(false);
            btn_execute.setVisible(true);
            tf_id.setText("");
            tf_nachname.setText("");
            tf_vorname.setText("");
            tf_adresse.setText("");
            tf_ausweisnummer.setText("");
            tf_email.setText("");
            tf_id.setEditable(true);
            tf_vorname.setEditable(true);
            tf_nachname.setEditable(true);
            tf_adresse.setEditable(true);
            tf_ausweisnummer.setEditable(true);
            tf_email.setEditable(true);
            

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_id.setVisible(true);
            tf_id.setVisible(true);
            btn_execute.setVisible(false);
            tf_id.setEditable(false);
            tf_vorname.setEditable(false);
            tf_nachname.setEditable(false);
            tf_adresse.setEditable(false);
            tf_ausweisnummer.setEditable(false);
            tf_email.setEditable(false);
            btn_update.setVisible(false);
            fill_JTextFields();
        }
    }

    public void getArraylistFromResultSet() {
        try {
            DBConnection db = new DBConnection();
            Connection conn = db.getConnection();
            Person at = new Person();
            personlist = new ArrayList();
            ResultSet rs = null;
            if(search){
               personlist = at.getAllPersonenSearch(search_ein);
                System.out.println(personlist.size());
            }else{
             rs = at.getAllPerson();
            while (rs.next()) {
                //id,vorname,nachname,ausweisnummer,email,adresse
                Person a = new Person();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setAusweisnummer(Integer.parseInt(rs.getObject(4).toString()));
                a.setEmail(rs.getObject(5).toString());
                a.setAdresse(rs.getObject(6).toString());
                personlist.add(a);
            }
            rs.close();
            }
            
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    public void fill_JTextFields() {
        getArraylistFromResultSet();
        anzahl_Datensaetze = personlist.size();
        if (anzahl_Datensaetze == 0) {
             tf_id.setText("");
            tf_vorname.setText("");
            tf_nachname.setText("");
            tf_email.setText("");
            tf_adresse.setText("");
            tf_ausweisnummer.setText("");
        }

        if (anzahl_Datensaetze > 0) {

            status_id = 0;
            lb_anzahldatensaetze.setText("" + anzahl_Datensaetze);
            help = status_id;
            help++;
            lb_aktuelles_Buch.setText("" + help);
            Person b = personlist.get(0);

            tf_id.setText("" + b.getId());
            tf_nachname.setText(b.getNachname());
            tf_vorname.setText(b.getVorname());
            tf_adresse.setText(b.getAdresse());
            tf_email.setText(b.getEmail());
            tf_ausweisnummer.setText("" + b.getAusweisnummer());
            
        }

    }

    public void fill_JTextFields(int id) {

        Person b = personlist.get(id);
        help = id;
        lb_aktuelles_Buch.setText("" + ++help);
        tf_id.setText("" + b.getId());
        tf_vorname.setText(b.getVorname());
        tf_nachname.setText(b.getNachname());
        tf_adresse.setText(b.getAdresse());
        tf_ausweisnummer.setText("" + b.getAusweisnummer());
        tf_email.setText(b.getEmail());
        

    }

    public void update() {
        System.out.println(update);
        if (update) {
            System.out.println(update);
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_id.setVisible(false);
            tf_id.setVisible(false);
            btn_update.setVisible(true);
            tf_nachname.setEditable(true);
            tf_vorname.setEditable(true);
            tf_adresse.setEditable(true);
            tf_ausweisnummer.setEditable(true);
            tf_email.setEditable(true);


        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_id.setVisible(true);
            tf_id.setVisible(true);
            btn_update.setVisible(false);
            tf_nachname.setEditable(false);
            tf_vorname.setEditable(false);
            tf_adresse.setEditable(false);
            tf_ausweisnummer.setEditable(false);
            tf_email.setEditable(false);
            fill_JTextFields();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn_forward) {
            if ((anzahl_Datensaetze - 1) > status_id) {
                status_id++;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_backward) {
            if (status_id > 0) {
                status_id--;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_delete) {
           if (JOptionPane.showConfirmDialog(null, "Soll diese Person gelöscht werden?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
 
            try {
                Person help = personlist.get(status_id);
                Person b = new Person();
                b.deletePerson(help);
                fill_JTextFields();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "help " + ex);
            }
           }
        } else if (e.getSource() == btn_newData) {
            
            hidden = true;
            hide();
        } else if (e.getSource() == btn_execute) {
            if((tf_vorname.getText().isEmpty() || tf_nachname.getText().isEmpty() || tf_ausweisnummer.getText().isEmpty() || tf_email.getText().isEmpty() || tf_adresse.getText().isEmpty()) == false){
            int counter = 0;
            try {
                //INSERT INTO person(vorname,nachname,ausweisnummer,email,adresse
                Person a = new Person(tf_vorname.getText(),tf_nachname.getText(),Integer.parseInt(tf_ausweisnummer.getText()),tf_email.getText(),tf_adresse.getText());
                a.insertPerson(a);
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                hidden = true;
            } else {
                hidden = false;
            }
            hide();
        }else{
                JOptionPane.showMessageDialog(null, "Füllen sie alle Felder aus!");
        }
        } else if (e.getSource() == btn_edit) {
            update = true;
            update();
        } else if (e.getSource() == btn_update) {
            int counter = 0;
            try {
                
                try {
                    //UPDATE Person set vorname = ? , nachname = ?,ausweisnummer = ?, email = ?,adresse = ? where id = ?
                    Person a = new Person(tf_vorname.getText(),tf_nachname.getText() ,Integer.parseInt(tf_ausweisnummer.getText()),tf_email.getText(),tf_adresse.getText());
                    a.setId(Integer.parseInt(tf_id.getText()));
                    a.updatePerson(a);
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                update = true;
            } else {
                update = false;
            }
            update();
        } else if (e.getSource() == btn_back) {
            hidden = false;
            hide();
        }else if(e.getSource() == btn_suche){
            
            System.out.println(tf_suchen.getText().isEmpty());
            if(tf_suchen.getText().isEmpty() == false){
            btn_suche.setVisible(false);
            tf_suchen.setVisible(false);
            btn_reset.setVisible(true);
            search = true;
                System.out.println(tf_suchen.getText());
            search_ein = tf_suchen.getText();
            }
            fill_JTextFields();
            search = false;
        }else if(e.getSource() == btn_search){
            btn_suche.setVisible(true);
            tf_suchen.setVisible(true);
            tf_suchen.requestFocus();
        }else if(e.getSource() == btn_reset){
           tf_suchen.setText("");
           fill_JTextFields();
           btn_reset.setVisible(false);
        }
    }

}
