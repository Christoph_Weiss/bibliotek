/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Views;

import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Person;
import biblio.bibliothek.Classes.Verlag;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Christoph Weiss
 */
public class Gui_Verlag extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JLabel lb_title;
    private JLabel lb_id;
    private JLabel lb_name;
    private JLabel lb_ort;
    private JLabel lb_anzahldatensaetze;
    private JLabel lb_aktuelles_Buch;
    

    private JTextField tf_id;
    private JTextField tf_name;
    private JTextField tf_ort;
    private JTextField tf_suchen;


    private JButton btn_newData;
    private JButton btn_search;
    private JButton btn_edit;
    private JButton btn_delete;
    private JButton btn_back;
    private JButton btn_backward;
    private JButton btn_forward;
    private JButton btn_execute;
    private JButton btn_update;
    private JButton btn_suche;
    private JButton btn_reset;

   
    private ArrayList<Verlag> verlaglist = new ArrayList();

    private int status_id;
    private int anzahl_Datensaetze;
    private int help;

    private boolean hidden;
    private boolean update;
    private boolean search;
    
    private String search_ein;

    public Gui_Verlag() {
        initalize();
        fill_JTextFields();
    }


    private void initalize() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("Verlag");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(250, 5, 500, 70);
        mainPanel.add(lb_title);
        
        tf_suchen = new JTextField();
        tf_suchen.setBounds(500,5,100,25);
        tf_suchen.setVisible(false);
        mainPanel.add(tf_suchen);
        
        btn_suche = new JButton();
        btn_suche.setText("GO");
        btn_suche.setVisible(false);
        btn_suche.addActionListener(this);
        btn_suche.setBounds(600, 5,70, 25);
        mainPanel.add(btn_suche);
        
        btn_reset = new JButton();
        btn_reset.setText("Reset");
        btn_reset.setVisible(false);
        btn_reset.addActionListener(this);
        btn_reset.setBounds(600, 50,70, 25);
        mainPanel.add(btn_reset);

        lb_id = new JLabel();
        lb_id.setText("ID:");
        lb_id.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_id.setBounds(50, 80, 150, 25);
        mainPanel.add(lb_id);

        tf_id = new JTextField();
        tf_id.setEditable(false);
        tf_id.setBounds(150, 80, 500, 25);
        mainPanel.add(tf_id);

        lb_name = new JLabel();
        lb_name.setText("Name:");
        lb_name.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_name.setBounds(50, 140, 200, 25);
        mainPanel.add(lb_name);

        tf_name = new JTextField();
        tf_name.setBounds(150, 140, 500, 25);
        tf_name.setEditable(false);
        mainPanel.add(tf_name);

        lb_ort = new JLabel();
        lb_ort.setText("Ort:");
        lb_ort.setFont(new Font("Serif", Font.PLAIN, 15));
        lb_ort.setBounds(50, 200, 200, 25);
        mainPanel.add(lb_ort);

        tf_ort = new JTextField();
        tf_ort.setBounds(150, 200, 500, 25);
        tf_ort.setEditable(false);
        mainPanel.add(tf_ort);

        

        btn_backward = new JButton();
        btn_backward.setText("←");
        btn_backward.setBounds(450, 425, 60, 60);
        btn_backward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_backward.addActionListener(this);
        mainPanel.add(btn_backward);
        
        btn_forward = new JButton();
        btn_forward.setText("→");
        btn_forward.setBounds(525, 425, 60, 60);
        btn_forward.setFont(new Font("Serif", Font.PLAIN, 15));
        btn_forward.addActionListener(this);
        mainPanel.add(btn_forward);

        lb_anzahldatensaetze = new JLabel();
        lb_anzahldatensaetze.setBounds(645, 425, 20, 40);
        mainPanel.add(lb_anzahldatensaetze);

        lb_aktuelles_Buch = new JLabel();
        lb_aktuelles_Buch.setBounds(620, 425, 20, 40);
        mainPanel.add(lb_aktuelles_Buch);

        File f1 = new File("");
        String path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\newData.png";
        ImageIcon img = new ImageIcon(path);
        btn_newData = new JButton(img);

        btn_newData.setOpaque(false);
        btn_newData.setContentAreaFilled(false);
        btn_newData.setBorderPainted(false);
        btn_newData.setBorder(BorderFactory.createEmptyBorder());
        btn_newData.setBounds(150, 500, 50, 50);
        btn_newData.addActionListener(this);
        mainPanel.add(btn_newData);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\search.png";
        img = new ImageIcon(path);
        btn_search = new JButton(img);

        btn_search.setOpaque(false);
        btn_search.setContentAreaFilled(false);
        btn_search.setBorderPainted(false);
        btn_search.setBorder(BorderFactory.createEmptyBorder());
        btn_search.setBounds(250, 500, 50, 50);
        btn_search.addActionListener(this);
        mainPanel.add(btn_search);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\edit.png";
        img = new ImageIcon(path);
        btn_edit = new JButton(img);

        btn_edit.setBorder(BorderFactory.createEmptyBorder());
        btn_edit.setOpaque(false);
        btn_edit.setContentAreaFilled(false);
        btn_edit.setBorderPainted(false);
        btn_edit.addActionListener(this);
        btn_edit.setBounds(350, 500, 50, 50);
        mainPanel.add(btn_edit);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\delete.png";
        img = new ImageIcon(path);
        btn_delete = new JButton(img);
        btn_delete.setBorder(BorderFactory.createEmptyBorder());
        btn_delete.setOpaque(false);
        btn_delete.setEnabled(false);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorderPainted(false);
        btn_delete.setBounds(450, 500, 50, 50);
        btn_delete.addActionListener(this);
        mainPanel.add(btn_delete);

        f1 = new File("");
        path = f1.getAbsolutePath();
        path += "\\src\\main\\java\\pic\\back.png";
        img = new ImageIcon(path);
        btn_back = new JButton(img);

        btn_back.setBorder(BorderFactory.createEmptyBorder());
        btn_back.setOpaque(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setBorderPainted(false);
        btn_back.addActionListener(this);
        btn_back.setBounds(550, 500, 50, 50);
        mainPanel.add(btn_back);

        btn_execute = new JButton();
        btn_execute.setText("Speichern");
        btn_execute.setBounds(440, 425, 200, 20);
        btn_execute.addActionListener(this);
        btn_execute.setVisible(false);
        mainPanel.add(btn_execute);

        btn_update = new JButton();
        btn_update.setText("Aktualisieren");
        btn_update.setBounds(440, 425, 200, 20);
        btn_update.addActionListener(this);
        btn_update.setVisible(false);
        mainPanel.add(btn_update);

    }

    public void hide() {
        if (hidden) {
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_id.setVisible(false);
            tf_id.setVisible(false);
            btn_execute.setVisible(true);
            tf_id.setText("");
            tf_name.setText("");
            tf_ort.setText("");
            tf_id.setEditable(true);
            tf_name.setEditable(true);
            tf_ort.setEditable(true);
            

        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_id.setVisible(true);
            tf_id.setVisible(true);
            btn_execute.setVisible(false);
            tf_id.setEditable(false);
            tf_name.setEditable(false);
            tf_ort.setEditable(false);
            btn_update.setVisible(false);
            fill_JTextFields();
        }
    }

    public void getArraylistFromResultSet() {
        try {
            DBConnection db = new DBConnection();
            Connection conn = db.getConnection();
            Verlag at = new Verlag();
            verlaglist = new ArrayList();
            ResultSet rs;
            if(search){
               verlaglist = at.getAllVerlaegeSearch(search_ein);
                System.out.println(verlaglist.size());
            }else{
             rs = at.getAllVerlaege();
            while (rs.next()) {
                
                Verlag a = new Verlag();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setName(rs.getObject(2).toString());
                a.setOrt(rs.getObject(3).toString());
                verlaglist.add(a);
            }
            rs.close();
            }     
            
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    public void fill_JTextFields() {
        getArraylistFromResultSet();
        anzahl_Datensaetze = verlaglist.size();
        if (anzahl_Datensaetze == 0) {
             tf_id.setText("");
            tf_name.setText("");
            tf_ort.setText("");

        }

        if (anzahl_Datensaetze > 0) {

            status_id = 0;
            lb_anzahldatensaetze.setText("" + anzahl_Datensaetze);
            help = status_id;
            help++;
            lb_aktuelles_Buch.setText("" + help);
            Verlag b = verlaglist.get(0);

            tf_id.setText("" + b.getId());
            tf_name.setText(b.getName());
            tf_ort.setText(b.getOrt());
            
        }

    }

    public void fill_JTextFields(int id) {

        Verlag b = verlaglist.get(id);
        help = id;
        lb_aktuelles_Buch.setText("" + ++help);
        tf_id.setText("" + b.getId());
        tf_name.setText(b.getName());
        tf_ort.setText(b.getOrt());

    }

    public void update() {
        System.out.println(update);
        if (update) {
            System.out.println(update);
            btn_backward.setVisible(false);
            btn_forward.setVisible(false);
            btn_newData.setVisible(false);
            lb_anzahldatensaetze.setVisible(false);
            lb_aktuelles_Buch.setVisible(false);
            btn_edit.setVisible(false);
            btn_delete.setVisible(false);
            btn_search.setVisible(false);
            lb_id.setVisible(false);
            tf_id.setVisible(false);
            btn_update.setVisible(true);
            tf_name.setEditable(true);
            tf_ort.setEditable(true);


        } else {
            btn_forward.setVisible(true);
            btn_backward.setVisible(true);
            btn_newData.setVisible(true);
            btn_edit.setVisible(true);
            btn_delete.setVisible(true);
            lb_anzahldatensaetze.setVisible(true);
            lb_aktuelles_Buch.setVisible(true);
            btn_search.setVisible(true);
            lb_id.setVisible(true);
            tf_id.setVisible(true);
            tf_name.setEditable(false);
            tf_ort.setEditable(false);
            btn_update.setVisible(false);
            fill_JTextFields();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn_forward) {
            if ((anzahl_Datensaetze - 1) > status_id) {
                status_id++;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_backward) {
            if (status_id > 0) {
                status_id--;
                fill_JTextFields(status_id);
            }
        } else if (e.getSource() == btn_delete) {
           if (JOptionPane.showConfirmDialog(null, "Soll dieser Verlag gelöscht werden?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
 
            try {
                Verlag help = verlaglist.get(status_id);
                Verlag b = new Verlag();
                try {
                    b.deleteVerlag(help);
                } catch (ParseException ex) {
                     JOptionPane.showMessageDialog(null, "help " + ex);
                }
                fill_JTextFields();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "help " + ex);
            }
           }
        } else if (e.getSource() == btn_newData) {
            
            hidden = true;
            hide();
        } else if (e.getSource() == btn_execute) {
            int counter = 0;
            if((tf_name.getText().isEmpty() || tf_ort.getText().isEmpty()) == false){
            try {
                Verlag a = new Verlag(tf_name.getText(), tf_ort.getText());
                try {
                    a.insertVerlag(a);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                hidden = true;
            } else {
                hidden = false;
            }
            hide();
            }else{
                JOptionPane.showMessageDialog(null, "Füllen sie alle Felder aus!!");
            }
        } else if (e.getSource() == btn_edit) {
            update = true;
            update();
        } else if (e.getSource() == btn_update) {
            int counter = 0;
            try {
                
                try {
                    Verlag a = new Verlag(tf_name.getText(), tf_ort.getText());
                    a.setId(Integer.parseInt(tf_id.getText()));
                    a.updateVerlag(a);
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                counter++;
            }
            if (counter == 1) {
                update = true;
            } else {
                update = false;
            }
            update();
        } else if (e.getSource() == btn_back) {
            hidden = false;
            hide();
        }else if(e.getSource() == btn_suche){
            
            System.out.println(tf_suchen.getText().isEmpty());
            if(tf_suchen.getText().isEmpty() == false){
            btn_suche.setVisible(false);
            tf_suchen.setVisible(false);
            btn_reset.setVisible(true);
            search = true;
                System.out.println(tf_suchen.getText());
            search_ein = tf_suchen.getText();
            }
            fill_JTextFields();
            search = false;
        }else if(e.getSource() == btn_search){
            btn_suche.setVisible(true);
            tf_suchen.setVisible(true);
            tf_suchen.requestFocus();
        }else if(e.getSource() == btn_reset){
           tf_suchen.setText("");
           fill_JTextFields();
           btn_reset.setVisible(false);
        }
    }

}
