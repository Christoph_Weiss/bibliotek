/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import biblio.bibliothek.Classes.Autor;
import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Buch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Christoph Weiss
 */
public class Buch_hat_autor {
    private int buch_id;
    private int autor_id;

    public Buch_hat_autor() {
    }

    public Buch_hat_autor(int buch_id, int autor_id) {
        this.buch_id = buch_id;
        this.autor_id = autor_id;
    }

    
    public int getBuch_id() {
        return buch_id;
    }

    public void setBuch_id(int buch_id) {
        this.buch_id = buch_id;
    }

    public int getAutor_id() {
        return autor_id;
    }

    public void setAutor_id(int autor_id) {
        this.autor_id = autor_id;
    }
    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }
    public void insertAutorWithBookWithObjects(Buch b, Autor a){

        try {
            
            Connection db = getConnectionForDB();
            String sql = "INSERT INTO buch_hat_autor(buch_id,autor_id) VALUES(?,?)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, b.getBuchnummer());
            statement.setInt(2,a.getId());
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void insertAutorWithBookWithIds(int buch_id, int autor_id){
               try {
            
            Connection db = getConnectionForDB();
            String sql = "INSERT INTO buch_hat_autor(buch_id,autor_id) VALUES(?,?)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            statement.setInt(2,autor_id);
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void updateAutorWithBook(int buch_id,int autor_id){
          try {
            
            Connection db = getConnectionForDB();
            String sql = "UPDATE buch_hat_autor SET buch_id, autor_id VALUES(?,?)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            statement.setInt(2,autor_id);
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
