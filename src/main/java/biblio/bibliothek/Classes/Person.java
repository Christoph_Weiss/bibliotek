/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Christoph Weiss
 */
public class Person {

    private int id;
    private String vorname;
    private String nachname;
    private int ausweisnummer;
    private String email;
    private String adresse;
    private ArrayList<Person> personsearchlist = new ArrayList<>();

    public Person(String vorname, String nachname, int ausweisnummer, String email, String adresse) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.ausweisnummer = ausweisnummer;
        this.email = email;
        this.adresse = adresse;
    }

    public Person() {
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public int getAusweisnummer() {
        return ausweisnummer;
    }

    public void setAusweisnummer(int ausweisnummer) {
        this.ausweisnummer = ausweisnummer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }

    public void insertPerson(Person b) throws ParseException {
        String vorname = b.getVorname();
        String nachname = b.getNachname();
        int ausweisnummer = b.getAusweisnummer();
        String email = b.getEmail();
        String adresse = b.getAdresse();
        try {
            Connection db = getConnectionForDB();
            String sql = "INSERT INTO person(vorname,nachname,ausweisnummer,email,adresse) VALUES(?,?,?,?,?)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setString(1, vorname);
            statement.setString(2, nachname);
            statement.setInt(3, ausweisnummer);
            statement.setString(4, email);
            statement.setString(5, adresse);
            statement.execute();
            statement.close();
            db.close();
            JOptionPane.showMessageDialog(null, "Person erfolgreich erstellt");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    public void deletePerson(int id) throws SQLException {
        PreparedStatement statement;
        try{
            Connection db = getConnectionForDB();
            String sql = "DELETE FROM Autor WHERE id=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            statement.execute();
            statement.close();
            db.close();

        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }

    }
    public void deletePerson(Person b) throws SQLException {
        PreparedStatement statement;
        try{
            Connection db = getConnectionForDB();
            String sql = "DELETE FROM Person WHERE id=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, b.getId());
            statement.execute();
            statement.close();
            db.close();
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }

    }
    public ResultSet getAllPerson() {
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,vorname,nachname,ausweisnummer,email,adresse from Person";
            Statement statement = db.createStatement();
            System.out.println(sql);
            rs = statement.executeQuery(sql);
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return rs;
    }
    public ArrayList getAllPersonenSearch(String search) {
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,vorname,nachname,ausweisnummer,email,adresse from Person Where vorname ILIKE  '%"+  search + "%' OR nachname ILIKE  '%"+  search + "%' OR email ILIKE  '%"+  search + "%' OR adresse  LIKE '%" + search +  "%'";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Person a = new Person();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setAusweisnummer(Integer.parseInt(rs.getObject(4).toString()));
                a.setEmail(rs.getObject(5).toString());
                a.setAdresse(rs.getObject(6).toString());
                personsearchlist.add(a);
            }
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return personsearchlist;
    }

    public Person getPerson(int id) {
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from Person where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                this.id = (rs.getInt("id"));
                this.vorname = (rs.getString("vorname"));
                this.nachname = (rs.getString("nachname"));
                this.ausweisnummer = (rs.getInt("ausweisnummer"));
                this.email = (rs.getString("email"));
                this.adresse = (rs.getString("adresse"));
                rs.close();
                db.close();
                statement.close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return this;
    }
    public void updatePerson(Person b) throws ParseException, SQLException{
        Connection db = getConnectionForDB();
            String sql = "UPDATE Person set vorname = ? , nachname = ?,ausweisnummer = ?, email = ?,adresse = ? where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setString(1, b.getVorname());
            statement.setString(2, b.getNachname());
            statement.setInt(3, b.getAusweisnummer());
            statement.setString(4, b.getEmail());
            statement.setString(5, b.getAdresse());
            statement.setInt(6, b.getId());
            statement.execute();
                db.close();
                statement.close();
    }

    public String toString() {
        return "ID  " + this.id + "\nVorname  " + this.vorname + "\nNachname  " + this.nachname + "\nAusweisnummer  " + this.ausweisnummer + "\nEmail  " + this.email + "\nAdresse  " + this.adresse +"\n";
    }
    
}
