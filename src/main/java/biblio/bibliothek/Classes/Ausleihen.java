/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import biblio.bibliothek.Classes.DBConnection;
import biblio.bibliothek.Classes.Person;
import biblio.bibliothek.Classes.Buch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Christoph Weiss
 */
public class Ausleihen {
    private int id;
    private int person_id;
    private int buch_id;
    private String ausleihdatum;
    private String zurueckgabedatum;
    private ArrayList<Ausleihen> zurueckgebenlist = new ArrayList<>();
    private ArrayList<Ausleihen> ausleihenAllsearch = new ArrayList<>();

    public Ausleihen() {
    }

  

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public int getBuch_id() {
        return buch_id;
    }

    public void setBuch_id(int buch_id) {
        this.buch_id = buch_id;
    }

    public String getAusleihdatum() {
        return ausleihdatum;
    }

    public void setAusleihdatum(String ausleihdatum) {
        this.ausleihdatum = ausleihdatum;
    }

    public String getZurueckgabedatum() {
        return zurueckgabedatum;
    }

    public void setZurueckgabedatum(String zurueckgabedatum) {
        this.zurueckgabedatum = zurueckgabedatum;
    }
    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }
    public ResultSet ausleihenAll(){
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,person_id,buch_id, buch_id,ausleihdatum,zurueckgabedatum from ausleihen where zurueckgabedatum IS NULL ";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
//    public ArrayList ausleihenAllSearch(String search){
//        ResultSet rs = null;
//        try {
//            Connection db = getConnectionForDB();
//            String sql = "SELECT id,person_id,buch_id, buch_id,ausleihdatum,zurueckgabedatum from ausleihen where zurueckgabedatum IS NULL  AND ";
//            Statement statement = db.createStatement();
//            rs = statement.executeQuery(sql);
//            while (rs.next()) {
//               
//                Ausleihen a = new Ausleihen();
//                a.setId(Integer.parseInt(rs.getObject(1).toString()));
//                a.setPerson_id(Integer.parseInt(rs.getObject(2).toString()));
//                a.setBuch_id(Integer.parseInt(rs.getObject(3).toString()));
//                a.setAusleihdatum(rs.getObject(4).toString());
//                a.setZurueckgabedatum(rs.getObject(5).toString());
//                ausleihenAllsearch.add(a);
//            }
//            rs.close();
//            db.close();
//        } catch (SQLException ex) {
//            JOptionPane.showMessageDialog(null, ex);
//        }
//        return ausleihenAllsearch;
//    }
    
    public Ausleihen Ausleihen(int ausleihen_id){
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from ausleihen where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, ausleihen_id);
            System.out.println(sql);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                System.out.println("buchnummerreal" + rs.getInt("buchnummer"));
                this.id = (rs.getInt("id"));
                this.person_id = (rs.getInt("person_id"));
                this.buch_id = (rs.getInt("buch_id"));
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                this.ausleihdatum = (dateFormat.format(rs.getDate("ausleihdatum")));
                this.zurueckgabedatum = (dateFormat.format(rs.getDate("zurueckgabedatum")));
                rs.close();
                db.close();
                statement.close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return this;
    }
    public ResultSet getAusgeliehen(Buch b){
           ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from ausleihen where buch_id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, b.getBuchnummer());
            rs = statement.executeQuery();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
    public ResultSet getAusgeliehen(int buch_id){
           ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from ausleihen where buch_id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            rs = statement.executeQuery();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
    public void deleteAusleihen(Buch b){
        PreparedStatement statement;
        try{
        Connection db = getConnectionForDB();
            String sql = "DELETE FROM ausleihen WHERE buch_id=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, b.getBuchnummer());
            statement.execute();
            statement.close();
            db.close();
            
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    public ResultSet ausgeliehenWithPersonWithObjekt(Person b){
           ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from ausleihen where person_id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, b.getId());
            rs = statement.executeQuery();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
    public ResultSet getAusgeliehenWithPersonID(int person_id){
           ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from ausleihen where person_id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, person_id);
            rs = statement.executeQuery();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
    public void insertBuchausleihen(Buch b,Person p) throws SQLException{
            Connection db = getConnectionForDB();
            int buch_id = b.getBuchnummer();
            int person_id = p.getId();
            String sql = "INSERT INTO ausleihen(person_id,buch_id,ausleihdatum,zurueckgabedatum) VALUES(?,?,NOW(),NULL)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, person_id);
            statement.setInt(2, buch_id);
            statement.execute();
            statement.close();
            sql = "UPDATE Buch Set ausgeliehen = true where buchnummer = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            statement.execute();
            statement.close();
            db.close();
    }
    public ArrayList getAllZurueckgeben(){
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,person_id,buch_id, buch_id,ausleihdatum,zurueckgabedatum from ausleihen where zurueckgabedatum IS NOT NULL ";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Ausleihen a = new Ausleihen();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setPerson_id(Integer.parseInt(rs.getObject(2).toString()));
                a.setBuch_id(Integer.parseInt(rs.getObject(3).toString()));
                a.setAusleihdatum(rs.getObject(4).toString());
                a.setZurueckgabedatum(rs.getObject(5).toString());
                zurueckgebenlist.add(a);
            }
            rs.close();
            db.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return zurueckgebenlist;
    }
    public void insertBuchausleihen(int buch_id,int schueler_id ) throws SQLException{
            Connection db = getConnectionForDB();
            String sql = "INSERT INTO ausleihen(person_id,buch_id,ausleihdatum,zurueckgabedatum) VALUES(?,?,NOW(),NULL)";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, schueler_id);
            statement.setInt(2, buch_id);
            statement.execute();
            statement.close();
            sql = "UPDATE Buch Set ausgeliehen = true where buchnummer = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            statement.execute();
            statement.close();
            db.close();
    }
    public void updateBuchzurueckgeben(int ausleih_id,int buch_id) throws SQLException{
        Connection db = getConnectionForDB();
        String sql = "UPDATE ausleihen set zurueckgabedatum = NOW() where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, ausleih_id);
            statement.execute();
            statement.close();
            sql = "UPDATE buch set ausgeliehen = false where buchnummer = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, buch_id);
            statement.execute();
            statement.close();
                db.close();
                
    }
    
}
