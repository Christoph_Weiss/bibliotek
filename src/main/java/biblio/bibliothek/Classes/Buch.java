/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Christoph Weiss
 */
public class Buch {

    private int buchnummer;
    private String sachgebiet;
    private String buchtitel;
    private String ort;
    private String erscheinungsjahr;
    private int verlag_id;
    private int autor_id;
    private ArrayList<Buch> ausleihenlist = new ArrayList();
    private ArrayList<Buch> searchlist = new ArrayList<>();
    
    public Buch(String sachgebiet, String buchtitel, String ort, String erscheinungsjahr, int verlag_id,int autor_id) {
        this.sachgebiet = sachgebiet;
        this.buchtitel = buchtitel;
        this.ort = ort;
        this.erscheinungsjahr = erscheinungsjahr;
        this.verlag_id = verlag_id;
        this.autor_id = autor_id;
    }

    public Buch() {

    }

    public int getBuchnummer() {
        return buchnummer;
    }

    public void setBuchnummer(int buchnummer) {
        this.buchnummer = buchnummer;
    }

    public String getSachgebiet() {
        return sachgebiet;
    }

    public void setSachgebiet(String sachgebiet) {
        this.sachgebiet = sachgebiet;
    }

    public String getBuchtitel() {
        return buchtitel;
    }

    public int getAutor_id() {
        return autor_id;
    }

    public void setAutor_id(int autor_id) {
        this.autor_id = autor_id;
    }

    public void setBuchtitel(String buchtitel) {
        this.buchtitel = buchtitel;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getErscheinungsjahr() {
        return erscheinungsjahr;
    }

    public void setErscheinungsjahr(String erscheinungsjahr) {
        this.erscheinungsjahr = erscheinungsjahr;
    }

    public int getVerlag_id() {
        return verlag_id;
    }

    public void setVerlag_id(int verlag_id) {
        this.verlag_id = verlag_id;
    }

    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }
    public ArrayList getAllBuecherSearch(String search){
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            //String sql = "SELECT sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,autor_id,buchnummer from Buch Where buchtitel LIKE  '%"+  search + "%' OR sachgebiet LIKE  '%"+  search + "%' OR ort LIKE  '%"+  search + "%' ";
            String sql = "SELECT sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,autor_id,buchnummer from Buch Where buchtitel ILIKE  '%"+  search + "%' OR sachgebiet ILIKE  '%"+  search + "%' OR ort ILIKE  '%"+  search + "%' OR CAST (erscheinungsjahr AS CHAR)  LIKE '% = " + search +  "%'";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Buch a = new Buch();
                a.setBuchnummer(Integer.parseInt(rs.getObject(7).toString()));
                a.setBuchtitel(rs.getObject(2).toString());
                a.setErscheinungsjahr((rs.getObject(4).toString()));
                a.setOrt(rs.getObject(3).toString());
                a.setSachgebiet(rs.getObject(1).toString());
                a.setVerlag_id(Integer.parseInt(rs.getObject(5).toString()));
                a.setAutor_id(Integer.parseInt(rs.getObject(6).toString()));
                searchlist.add(a);
            }
            rs.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }
        
        return searchlist;
    }

    public void insertBuch(Buch b) throws ParseException {
        String sachgebiet_ein = b.getSachgebiet();
        String buchtitel_ein = b.getBuchtitel();
        String ort_ein = b.getOrt();
        Date erscheinungsjahr_ein = new SimpleDateFormat("yyyy-MM-dd").parse(b.getErscheinungsjahr());
        int verlag_id_ein = b.getVerlag_id();
        int autor_id_ein = b.getAutor_id();

        try ( Connection db = getConnectionForDB()) {
            String sql = "INSERT INTO buch(sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,autor_id) VALUES(?,?,?,?,?,?)";
            try ( PreparedStatement statement = db.prepareStatement(sql)) {
                statement.setString(1, sachgebiet_ein);
                statement.setString(2, buchtitel_ein);
                statement.setString(3, ort_ein);
                statement.setDate(4, new java.sql.Date(erscheinungsjahr_ein.getTime()));
                statement.setInt(5, verlag_id_ein);
                statement.setInt(6, autor_id_ein);
                statement.execute();
                statement.close();
                db.close();
                JOptionPane.showMessageDialog(null, "Buch erfolgreich erstellt");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }

    }

    public void insertBuch(String sachgebiet, String buchtitel, String ort, String erscheinungsjahr, int verlag_id,int autor_id,boolean ausgeliehen) throws ParseException {
        Date erscheinungsjahr_ein = new SimpleDateFormat("yyyy-MM-dd").parse(erscheinungsjahr);
        try ( Connection db = getConnectionForDB()) {
            String sql = "INSERT INTO buch(sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,autor_id,ausgeliehen) VALUES(?,?,?,?,?,?,?)";
            try ( PreparedStatement statement = db.prepareStatement(sql)) {
                statement.setString(1, sachgebiet);
                statement.setString(2, buchtitel);
                statement.setString(3, ort);
                statement.setDate(4, new java.sql.Date(erscheinungsjahr_ein.getTime()));
                statement.setInt(5, verlag_id);
                statement.setInt(6, autor_id);
                statement.setBoolean(7, ausgeliehen);
                System.out.println(statement);
                statement.execute();
                statement.close();
                db.close();
                JOptionPane.showMessageDialog(null, "Buch erfolgreich erstellt");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }
    }

    public ResultSet getAllBuecher() {
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT sachgebiet,buchtitel,ort,erscheinungsjahr,verlag_id,autor_id,buchnummer from Buch";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }
        return rs;
    }
    public Buch getBuch(int buchnummer) {
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from Buch where buchnummer = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, buchnummer);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                System.out.println("buchnummerreal" + rs.getInt("buchnummer"));
                this.buchnummer = (rs.getInt("buchnummer"));
                this.sachgebiet = (rs.getString("sachgebiet"));
                System.out.println(rs.getString("sachgebiet"));
                this.buchtitel = (rs.getString("buchtitel"));
                this.ort = (rs.getString("ort"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                this.erscheinungsjahr = (dateFormat.format(rs.getDate("erscheinungsjahr")));
                this.verlag_id = (rs.getInt("verlag_id"));
                this.autor_id = (rs.getInt("autor_id"));
                rs.close();
                db.close();
                statement.close();
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }
        return this;
    }

    public void updateBuch(Buch b) throws ParseException, SQLException {
        PreparedStatement statement;
        try ( Connection db = getConnectionForDB()) {
            String sql = "UPDATE Buch set sachgebiet = ? , buchtitel = ?,ort = ?, erscheinungsjahr = ?,verlag_id = ?, autor_id = ? where buchnummer = ?";
            statement = db.prepareStatement(sql);
            statement.setString(1, b.getSachgebiet());
            statement.setString(2, b.getBuchtitel());
            statement.setString(3, b.getOrt());
            statement.setDate(4, new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(b.getErscheinungsjahr()).getTime()));
            statement.setInt(5, b.getVerlag_id());
            statement.setInt(6, b.getAutor_id());
            statement.setInt(7, b.getBuchnummer());
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }

    }
    public ArrayList getAllNotAusgeliehen(){
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT buchnummer,buchtitel from buch where ausgeliehen=false";
            Statement statement = db.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Buch a = new Buch();
                a.setBuchnummer(Integer.parseInt(rs.getObject(1).toString()));
                a.setBuchtitel(rs.getObject(2).toString());
                ausleihenlist.add(a);
            }
            db.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ausleihenlist;
    }
    public void updateBuch(String sachgebiet, String buchtitel, String ort, String erscheinungjahr, int verlag_id,int autor_id, int buchnummer) throws ParseException, SQLException {
        PreparedStatement statement;
        try {
            Connection db = getConnectionForDB();
            String sql = "UPDATE Buch set sachgebiet = ? , buchtitel = ?,ort = ?, erscheinungsjahr = ?,verlag_id = ?, autor_id = ? where buchnummer = ?";
            statement = db.prepareStatement(sql);
            statement.setString(1, sachgebiet);
            statement.setString(2, buchtitel);
            statement.setString(3, ort);
            statement.setDate(4, new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(erscheinungjahr).getTime()));
            statement.setInt(5, verlag_id);
            statement.setInt(6, autor_id);
            statement.setInt(7, buchnummer);
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }

    }

    public void deleteBuchWithObject(Buch b) throws SQLException {
        PreparedStatement statement;
        try {
            Connection db = getConnectionForDB();
            String sql = "DELETE FROM Buch WHERE buchnummer=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, b.getBuchnummer());
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler" + ex);
        }

    }

    public void deleteBuchWithId(int id) throws SQLException {
        PreparedStatement statement;
        try ( Connection db = getConnectionForDB()) {
            String sql = "DELETE FROM Buch WHERE buchnummer=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            statement.execute();
            statement.close();
            db.close();
        }

    }

    public String toString() {
        return "Buchnummer \n " + this.buchnummer + "Sachgebiet \n " + this.sachgebiet + "Buchtitel \n " + this.buchtitel + "Ort \n " + this.ort + "Erscheinungsjahr \n " + this.erscheinungsjahr + "Verlag_id \n " + this.verlag_id;
    }

}
