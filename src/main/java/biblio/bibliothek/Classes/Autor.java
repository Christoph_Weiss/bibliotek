/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Christoph Weiss
 */
public class Autor {

    private int id;
    private String vorname;
    private String nachname;
    private String geburtsort;
    private String geburtsdatum;
    private ArrayList<Autor>  autorlistsearch = new ArrayList<>();

    public Autor(String vorname, String nachname, String geburtsort, String geburtsdatum) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsort = geburtsort;
        this.geburtsdatum = geburtsdatum;
    }

    public Autor() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getGeburtsort() {
        return geburtsort;
    }

    public void setGeburtsort(String geburtsort) {
        this.geburtsort = geburtsort;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }

    public void insertAutor(Autor b) throws ParseException {
        String vorname_ein = b.getVorname();
        String nachname_ein = b.getNachname();
        String geburtsort_ein = b.getGeburtsort();
        Date geburtsdatum_ein = new SimpleDateFormat("yyyy-MM-dd").parse(b.getGeburtsdatum());
        try {
            try ( Connection db = getConnectionForDB()) {
                String sql = "INSERT INTO autor(vorname,nachname,geburtsort,geburtsdatum) VALUES(?,?,?,?)";
                try ( PreparedStatement statement = db.prepareStatement(sql)) {
                    statement.setString(1, vorname_ein);
                    statement.setString(2, nachname_ein);
                    statement.setString(3, geburtsort_ein);
                    statement.setDate(4, new java.sql.Date(geburtsdatum_ein.getTime()));
                    statement.execute();
                    statement.close();
                }
                JOptionPane.showMessageDialog(null, "Autor erfolgreich erstellt");
                db.close();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }

    }
    public ArrayList getAllAutorSearch(String search){
        ResultSet rs = null;
        Statement s = null;
         try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,vorname,nachname,geburtsort,geburtsdatum from Autor where vorname ILIKE '%" + search + "%' OR nachname ILIKE '%" + search + "%' OR geburtsort ILIKE '%" + search + "%'";
            s = db.createStatement();
            rs = s.executeQuery(sql);
            while (rs.next()) {
                
                Autor a = new Autor();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setVorname(rs.getObject(2).toString());
                a.setNachname(rs.getObject(3).toString());
                a.setGeburtsort(rs.getObject(4).toString());
                a.setGeburtsdatum(rs.getObject(5).toString());
                autorlistsearch.add(a);
            }
            rs.close();

            db.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }
        return autorlistsearch;
    }
    public ResultSet getAllAutor() {
        ResultSet rs = null;
        Statement s = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,vorname,nachname,geburtsort,geburtsdatum from Autor";
            s = db.createStatement();
            rs = s.executeQuery(sql);
            db.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }
        
        return rs;
        
    }

    public void deleteAutor(int id) throws SQLException {
        PreparedStatement statement;
        try{
            Connection db = getConnectionForDB();
            String sql = "DELETE FROM Autor WHERE id=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            statement.execute();
            statement.close();
            db.close();

        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }

    }
    public void deleteAutor(Autor b) throws SQLException {
        PreparedStatement statement;
        try{
            Connection db = getConnectionForDB();
            String sql = "DELETE FROM Autor WHERE id=?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, b.getId());
            statement.execute();
            statement.close();
            db.close();
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }

    }

    public void insertAutor(String vorname, String nachname, String geburtsort, String geburtsdatum_ein) throws ParseException {
        Date geburtsdatum = new SimpleDateFormat("yyyy-MM-dd").parse(geburtsdatum_ein);
        try {
            try ( Connection db = getConnectionForDB()) {
                String sql = "INSERT INTO autor(vorname,nachname,geburtsort,geburtsdatum) VALUES(?,?,?,?)";
                PreparedStatement statement = db.prepareStatement(sql);
                statement.setString(1, vorname);
                statement.setString(2, nachname);
                statement.setString(3, geburtsort);
                statement.setDate(4, new java.sql.Date(geburtsdatum.getTime()));
                statement.execute();
                statement.close();
                db.close();
                JOptionPane.showMessageDialog(null, "Autor erfolgreich erstellt");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }
    }

    public Autor getAutor(int id) {
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from Autor where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            System.out.println(sql);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                this.id = (rs.getInt("id"));
                this.vorname = (rs.getString("vorname"));
                this.nachname = (rs.getString("nachname"));
                this.geburtsort = (rs.getString("geburtsort"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                this.geburtsdatum = (dateFormat.format(rs.getDate("geburtsdatum")));
                rs.close();
                db.close();
                statement.close();
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }
        return this;
    }

    public void updateAutor(Autor b) throws ParseException, SQLException {
        try{
        Connection db = getConnectionForDB();
        String sql = "UPDATE Autor set vorname = ? , nachname = ?,geburtsort = ?, geburtsdatum = ? where id = ?";
        PreparedStatement statement = db.prepareStatement(sql);
        statement.setString(1, b.getVorname());
        statement.setString(2, b.getNachname());
        statement.setString(3, b.getGeburtsort());
        statement.setDate(4, new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(b.getGeburtsdatum()).getTime()));
        statement.setInt(5, b.getId());
        statement.execute();
        db.close();
        statement.close();
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Fehler " + ex);
        }
    }

    public String toString() {
        return "ID  " + this.id + "\nVorname  " + this.vorname + "\nNachname  " + this.nachname + "\nGeburtsort  " + this.geburtsort + "\nGeburtsdatum  " + this.geburtsdatum + "\n";
    }

}
