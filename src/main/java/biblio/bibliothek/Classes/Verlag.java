/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Christoph Weiss
 */
public class Verlag {

    private int id;
    private String name;
    private String ort;
    private ArrayList<Verlag> verlagsearchlist = new ArrayList<>();

    public Verlag(String name, String ort) {
        this.name = name;
        this.ort = ort;

    }

    public Verlag() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public Connection getConnectionForDB() {
        DBConnection db = new DBConnection();
        return db.getConnection();
    }

    public void insertVerlag(Verlag b) throws ParseException, SQLException {
        int id_ein = b.getId();
        String name_ein = b.getName();
        String ort_ein = b.getOrt();
        try {
            try ( Connection db = getConnectionForDB()) {
                String sql = "INSERT INTO Verlag(name,ort) VALUES(?,?)";
                try ( PreparedStatement statement = db.prepareStatement(sql)) {
                    statement.setString(1, name_ein);
                    statement.setString(2, ort_ein);
                    statement.execute();
                    statement.close();
                    JOptionPane.showMessageDialog(null, "Verlag erfolgreich erstellt");
                }
                db.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void insertVerlag(int id_ein, String name_ein, String ort_ein) throws ParseException, SQLException {
        try {
            try ( Connection db = getConnectionForDB()) {
                String sql = "INSERT INTO Verlag(name,ort) VALUES(?,?)";
                try ( PreparedStatement statement = db.prepareStatement(sql)) {
                    statement.setString(1, name_ein);
                    statement.setString(2, ort_ein);
                    statement.execute();
                    statement.close();
                    JOptionPane.showMessageDialog(null, "Verlag erfolgreich erstellt");
                }
                db.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public ResultSet getAllVerlaege() {
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,name,ort from Verlag";
            Statement statement = db.createStatement();
            System.out.println(sql);
            rs = statement.executeQuery(sql);

            db.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rs;
    }
    public ArrayList getAllVerlaegeSearch(String search) {
        ResultSet rs = null;
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT id,name,ort from Verlag where name ILIKE '%" + search + "%' OR ort ILIKE '%" + search + "%'";
            Statement statement = db.createStatement();
            System.out.println(sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Verlag a = new Verlag();
                a.setId(Integer.parseInt(rs.getObject(1).toString()));
                a.setName(rs.getObject(2).toString());
                a.setOrt(rs.getObject(3).toString());
                verlagsearchlist.add(a);
            }
            rs.close();
            db.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return verlagsearchlist;
    }

    public Verlag getVerlag(int id) {
        try {
            Connection db = getConnectionForDB();
            String sql = "SELECT * from Verlag where id = ?";
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setInt(1, id);
            System.out.println(sql);
            ResultSet rs = statement.executeQuery();
            //System.out.println(rs.getRe);
            if (rs.next()) {
                this.id = (rs.getInt("id"));
                this.name = (rs.getString("name"));
                this.ort = (rs.getString("ort"));
                rs.close();
                db.close();
                statement.close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return this;
    }

    public void updateVerlag(Verlag b) throws ParseException, SQLException {
        PreparedStatement statement;
        try ( Connection db = getConnectionForDB()) {
            String sql = "UPDATE Verlag set name = ? , ort = ? where id = ?";
            statement = db.prepareStatement(sql);
            statement.setString(1, b.getName());
            statement.setString(2, b.getOrt());
            statement.setInt(3, b.getId());
            statement.execute();
            statement.close();
            db.close();
        }

    }

    public void updateVerlag(String name_ein, String ort_ein, int id_ein) throws ParseException, SQLException {
        PreparedStatement statement;
        try ( Connection db = getConnectionForDB()) {
            String sql = "UPDATE Verlag set name = ? , ort = ? where id = ?";
            statement = db.prepareStatement(sql);
            statement.setString(1, name_ein);
            statement.setString(2, ort_ein);
            statement.setInt(3, id_ein);
            statement.execute();
            statement.close();
            db.close();
        }

    }

    public void deleteVerlag(Verlag b) throws ParseException, SQLException {
        try {
            Connection db = getConnectionForDB();
            PreparedStatement statement;
            String sql = "Delete FROM Verlag where id = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, b.getId());
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    public void deleteVerlag(int id_ein) throws ParseException, SQLException {
        try {
            Connection db = getConnectionForDB();
            PreparedStatement statement;
            String sql = "UPDATE Buch SET verlag_id = NULL where verlag_id = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, id_ein);
            statement.execute();
            statement.close();
            
            sql = "Delete FROM Verlag where id = ?";
            statement = db.prepareStatement(sql);
            statement.setInt(1, id_ein);
            statement.execute();
            statement.close();
            db.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    public String toString() {
        return "ID  " + this.id + "\nName  " + this.name + "\nOrt  " + this.ort + "\n";
    }

}
