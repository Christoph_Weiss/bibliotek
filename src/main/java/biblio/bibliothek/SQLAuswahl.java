/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblio.bibliothek;

import biblio.bibliothek.Views.Gui_bucheigenschaften;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Christoph Weiss
 */
public class SQLAuswahl extends JFrame implements ActionListener  {

    private JPanel mainPanel;
    private JLabel lb_title;
    private JButton btn_sachgebietsuche;
    private JButton btn_buchtitelsuche;
    private JButton btn_Autorsuche;
    private JButton btn_ausleiher;
    private JButton btn_ausleihegroesser3Monate;
    private JButton btn_buchnummersuchen;
    private JButton btn_zurueckzumHauptmenu;
    
    public SQLAuswahl() {
        initalize();
    }

    private void initalize() {
        this.setSize(new Dimension(700, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        mainPanel = new JPanel();
                
        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        lb_title = new JLabel();
        lb_title.setText("Abfrage-Untermenu");
        lb_title.setFont(new Font("Serif",Font.PLAIN,50));
        lb_title.setBounds(155,10,500,50);
        mainPanel.add(lb_title);
        
        btn_sachgebietsuche = new JButton();
        btn_sachgebietsuche.setText("Sachgebietssuche");
        btn_sachgebietsuche.setForeground(Color.black);
        btn_sachgebietsuche.setBackground(Color.white);
        btn_sachgebietsuche.addActionListener(this);
        btn_sachgebietsuche.setFont(new Font("Serif",Font.PLAIN,20));
        btn_sachgebietsuche.setBounds(150, 80, 200, 50);
        mainPanel.add(btn_sachgebietsuche);
        
        btn_buchtitelsuche = new JButton();
        btn_buchtitelsuche.setText("Buchtitelsuche");
        btn_buchtitelsuche.setForeground(Color.black);
        btn_buchtitelsuche.setBackground(Color.white);
        btn_buchtitelsuche.addActionListener(this);
        btn_buchtitelsuche.setFont(new Font("Serif",Font.PLAIN,20));
        btn_buchtitelsuche.setBounds(370, 80, 200, 50);
        mainPanel.add(btn_buchtitelsuche);
        
        btn_Autorsuche = new JButton();
        btn_Autorsuche.setText("Autorensuche");
        btn_Autorsuche.setForeground(Color.black);
        btn_Autorsuche.setBackground(Color.white);
        btn_Autorsuche.addActionListener(this);
        btn_Autorsuche.setFont(new Font("Serif",Font.PLAIN,20));
        btn_Autorsuche.setBounds(150, 160, 200, 50);
        mainPanel.add(btn_Autorsuche);
        
        btn_ausleiher = new JButton();
        btn_ausleiher.setText("Ausleiher suchen");
        btn_ausleiher.setForeground(Color.black);
        btn_ausleiher.setBackground(Color.white);
        btn_ausleiher.addActionListener(this);
        btn_ausleiher.setFont(new Font("Serif",Font.PLAIN,20));
        btn_ausleiher.setBounds(370, 160, 200, 50);
        mainPanel.add(btn_ausleiher);
        
        btn_ausleihegroesser3Monate = new JButton();
        btn_ausleihegroesser3Monate.setText("Ausleihe > 3 Monate");
        btn_ausleihegroesser3Monate.setForeground(Color.black);
        btn_ausleihegroesser3Monate.setBackground(Color.white);
        btn_ausleihegroesser3Monate.addActionListener(this);
        btn_ausleihegroesser3Monate.setFont(new Font("Serif",Font.PLAIN,20));
        btn_ausleihegroesser3Monate.setBounds(150, 240, 200, 50);
        mainPanel.add(btn_ausleihegroesser3Monate);
        
        btn_buchnummersuchen = new JButton();
        btn_buchnummersuchen.setText("Buchnummer suchen");
        btn_buchnummersuchen.setForeground(Color.black);
        btn_buchnummersuchen.setBackground(Color.white);
        btn_buchnummersuchen.addActionListener(this);
        btn_buchnummersuchen.setFont(new Font("Serif",Font.PLAIN,20));
        btn_buchnummersuchen.setBounds(370, 240, 200, 50);
        mainPanel.add(btn_buchnummersuchen);
        
        btn_zurueckzumHauptmenu = new JButton();
        btn_zurueckzumHauptmenu.setText("Zurück zum Hauptmenu");
        btn_zurueckzumHauptmenu.setForeground(Color.black);
        btn_zurueckzumHauptmenu.setBackground(Color.white);
        btn_zurueckzumHauptmenu.addActionListener(this);
        btn_zurueckzumHauptmenu.setFont(new Font("Serif",Font.PLAIN,20));
        btn_zurueckzumHauptmenu.setBounds(155, 320, 400, 50);
        mainPanel.add(btn_zurueckzumHauptmenu);
        
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == btn_sachgebietsuche){
            System.out.println("Sachgebietssuche");
        }else if(e.getSource() == btn_buchtitelsuche){
            Gui_bucheigenschaften buchtitel = new Gui_bucheigenschaften();
            buchtitel.setVisible(true);
            System.out.println("Buchtitelsuche");
        }else if(e.getSource() == btn_Autorsuche){
            System.out.println("Autorensuche");
        }else if(e.getSource() == btn_ausleiher){
            System.out.println("Ausleiher suchen");
        }else if(e.getSource() == btn_ausleihegroesser3Monate){
            System.out.println("Ausleihe > 3 Monate");
        }else if(e.getSource() == btn_buchnummersuchen){
            System.out.println("Buchnummer suchen");
        }
    }
    

}
